const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const AssetsManifest = require('webpack-assets-manifest');

const { cssModulesConfig } = require('./pipeline/configs/css-modules.config');
const { cssConfig } = require('./pipeline/configs/css.config');
const { fileLoaderConfig } = require('./pipeline/configs/file-loader.config');
const { urlLoaderConfig } = require('./pipeline/configs/url-loader.config');
const { scssConfig } = require('./pipeline/configs/scss.config');
const { getGltfAssetManifset } = require('./pipeline/functions/get-gltf-asset-manifest');
const { srcAssets3DPath, dstAssets3DPath } = require('./pipeline/consts/paths');

module.exports.webpackBaseConfig = {
  entry: ['./src/index.tsx'],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name]-[hash].js'
  },
  module: {
    rules: [cssModulesConfig, cssConfig, scssConfig, fileLoaderConfig, urlLoaderConfig]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js']
  },
  plugins: [
    new CopyPlugin({
      patterns: [{ from: 'public' }]
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      filename: 'index.html',
      favicon: './src/assets/static/images/icon.svg',
      title: 'Tic-tac-toe'
    }),
    new AssetsManifest({
      output: 'app-asset-manifest.json',
      assets: {
        'icon.svg': 'icon.svg',
        '/': '/'
      },
      transform: assets => assets
    }),
    new AssetsManifest({
      output: 'gltf-asset-manifest.json',
      customize: () => false,
      transform: () =>
        getGltfAssetManifset(dstAssets3DPath, [path.join(srcAssets3DPath, 'default')])
    })
  ]
};
