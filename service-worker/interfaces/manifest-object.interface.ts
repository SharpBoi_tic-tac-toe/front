export interface ManifestObject {
  [x: string]: string;
}
