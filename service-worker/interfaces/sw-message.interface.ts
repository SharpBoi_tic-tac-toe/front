//  eslint-disable-next-line spaced-comment
/// <reference lib="webworker" />

export interface SWMessageData<T = void> {
  type: string;
  payload: T;
}
export interface SWMessageEvent extends ExtendableMessageEvent {
  data: SWMessageData<unknown>;
}
