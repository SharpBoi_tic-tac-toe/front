// eslint-disable-next-line spaced-comment
/// <reference lib="webworker" />
// eslint-disable-next-line no-shadow
declare let self: ServiceWorkerGlobalScope;
export {};
export const SW_HASH = $SW_HASH;

import { SWMessenger } from './classes/sw-messenger';
import {
  appManifest,
  APP_CACHE,
  asset3DPathRegexp,
  devServerRegexp,
  gltfManifest,
  GLTF_CACHE,
  MANIFEST_CACHE,
  yaMetrikaRegexp
} from './consts';
import { interceptAppFetch } from './functions/intercept-app-fetch.function';
import { interceptGltfFetch } from './functions/intercept-gltf-fetch.function';
import { storeManifest } from './functions/store-manifest.function';
import { syncCache } from './functions/sync-cache.function';
import { syncIndex } from './functions/sync-index.function';
import { SWMessageEvent } from './interfaces/sw-message.interface';

const messenger = new SWMessenger(self);

// INSTALL
self.addEventListener('install', e => {
  e.waitUntil(
    (async () => {
      await self.skipWaiting();

      await storeManifest(gltfManifest);
      await storeManifest(appManifest);

      await syncCache(APP_CACHE, appManifest);
      await syncCache(GLTF_CACHE, gltfManifest);
      await syncIndex();

      console.log('sw store sync success');
      console.log('sw install');
    })()
  );
});

// ACTIVATE
self.addEventListener('activate', e => {
  e.waitUntil(
    (async () => {
      const cacheWhitelist = [APP_CACHE, GLTF_CACHE, MANIFEST_CACHE];

      const keyList = await caches.keys();
      const keysDeletitions = keyList
        .filter(key => !cacheWhitelist.includes(key))
        .map(key => caches.delete(key));

      await Promise.all(keysDeletitions);
      console.log('sw activate');
    })()
  );
});

// FETCH
self.addEventListener('fetch', e => {
  e.respondWith(
    (async () => {
      if (yaMetrikaRegexp.test(e.request.url) || devServerRegexp.test(e.request.url)) {
        return fetch(e.request);
      }

      if (asset3DPathRegexp.test(e.request.url)) {
        return interceptGltfFetch(e.request);
      }

      return interceptAppFetch(e.request);
    })()
  );
});

// MESSAGES
self.addEventListener('message', (e: SWMessageEvent) => {
  messenger.receiveAll(e.data);
});
