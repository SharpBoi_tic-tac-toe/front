import { MessageCreator } from '../functions/create-message.function';
import { SWMessageData } from '../interfaces/sw-message.interface';

type OnMessageCallback<T> = (msg: SWMessageData<T>) => void;

export class SWMessenger {
  private readonly messagesMap: Map<string, OnMessageCallback<any>[]> = new Map();

  constructor(private readonly swCtx: ServiceWorkerGlobalScope) {}

  public receiveAll(messageData: SWMessageData<unknown>): void {
    const { type } = messageData;
    if (!this.messagesMap.has(type)) {
      console.warn(`SW no callback for type ${type}`);
      return;
    }

    const callbacks = this.messagesMap.get(type);
    callbacks.forEach(callback => callback(messageData));
  }

  public onMessage<Tpayload>(
    message: MessageCreator<Tpayload>,
    onMessage: OnMessageCallback<Tpayload>
  ): void {
    const { type } = message;

    if (!this.messagesMap.has(type)) {
      this.messagesMap.set(type, []);
    }

    const callbacks = this.messagesMap.get(type);
    callbacks.push(onMessage);
    this.messagesMap.set(type, callbacks);
  }
  public postMessage(msg: SWMessageData<unknown>): void {
    void this.swCtx.clients.matchAll().then(clients =>
      clients.forEach(client => {
        client.postMessage(msg);
      })
    );
  }
}
