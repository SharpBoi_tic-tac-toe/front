export const indexPath = '/';

export const asset3DPathRegexp = new RegExp(/assets\/3d\//i);
export const yaMetrikaRegexp = new RegExp(/:\/\/mc\.yandex\.ru/i);
export const devServerRegexp = new RegExp(/\/sockjs-node\/info\?/i);

export const APP_CACHE = 'app-cache-v1';
export const GLTF_CACHE = 'gltf-cache-v1';
export const MANIFEST_CACHE = 'manifest-cache-v1';

export const appManifest = 'app-asset-manifest.json';
export const gltfManifest = 'gltf-asset-manifest.json';
