import { createMessage } from '../functions/create-message.function';

export const activateEnd = createMessage('MSG_ACTIVATE_END');
export const startSyncCache = createMessage('MSG_START_SYNC_CACHE');
export const endSyncCache = createMessage('MSG_END_SYNC_CACHE');
export const anyFetch = createMessage('MSG_ANY_FETCH');
