import { APP_CACHE, indexPath } from '../consts';

export async function syncIndex(): Promise<void> {
  const cache = await caches.open(APP_CACHE);
  const index = await fetch(indexPath);
  await cache.put(indexPath, index);
  console.log('sw index resync');
}
