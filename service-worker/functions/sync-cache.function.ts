import path from 'path';
import url from 'url';
import { MANIFEST_CACHE } from '../consts';
import { ManifestObject } from '../interfaces/manifest-object.interface';

export async function syncCache(cacheName: string, withManifestName: string): Promise<void> {
  const manifestCache = await caches.open(MANIFEST_CACHE);
  const cache = await caches.open(cacheName);
  const manifest: ManifestObject = await (await manifestCache.match(withManifestName)).json();
  const requests = await cache.keys();

  const newAssetsPaths = Object.values(manifest).map(assetPath => path.join('/', assetPath));
  const oldAssetsPaths = requests.map(req => url.parse(req.url)).map(query => query.path);
  const oldAssetsToDelete = oldAssetsPaths.filter(oldAsset => !newAssetsPaths.includes(oldAsset));
  const newAssetsToAdd = newAssetsPaths.filter(newAsset => !oldAssetsPaths.includes(newAsset));

  for (const oldPath of oldAssetsToDelete) {
    await cache.delete(oldPath);
  }
  for (const newPath of newAssetsToAdd) {
    await cache.add(newPath);
  }

  if (oldAssetsToDelete.length > 0) {
    console.log('sw delete old', oldAssetsToDelete);
  }
  if (newAssetsToAdd.length > 0) {
    console.log('sw add new  ', newAssetsToAdd);
  }
}
