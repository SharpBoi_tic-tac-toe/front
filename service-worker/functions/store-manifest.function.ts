import { MANIFEST_CACHE } from '../consts';

export async function storeManifest(manifestUrl: string): Promise<void> {
  const cache = await caches.open(MANIFEST_CACHE);
  const newManifest = await fetch(manifestUrl);
  await cache.put(manifestUrl, newManifest);
}
