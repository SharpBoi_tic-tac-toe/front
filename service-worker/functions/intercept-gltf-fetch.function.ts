import { gltfManifest } from '../consts';
import { ManifestObject } from '../interfaces/manifest-object.interface';

export async function interceptGltfFetch(req: Request): Promise<Response> {
  const requestFile = String(req.url).split('/').pop();
  const manifest: ManifestObject = await caches.match(gltfManifest).then(res => res.json());
  const assetPath = manifest[requestFile];

  return caches.match(assetPath);
}
