export async function interceptAppFetch(req: Request): Promise<Response> {
  const res = await caches.match(req);
  return res || fetch(req);
}
