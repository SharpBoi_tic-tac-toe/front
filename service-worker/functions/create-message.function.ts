import { SWMessageData } from '../interfaces/sw-message.interface';

export interface MessageCreator<T> {
  type: string;
  (payload: T): SWMessageData<T>;
}
export function createMessage<T = void>(type: string): MessageCreator<T> {
  const creator: MessageCreator<T> = (payload: T) => ({
    type,
    payload
  });
  creator.type = type;
  return creator;
}
