/**
 *
 * @param {'prod' | 'dev'} useConfigType
 */
module.exports.createTsxConfig = function (useConfigType) {
  return {
    test: /\.ts(x)?$/,
    loader: 'ts-loader',
    options: {
      configFile: useConfigType === 'prod' ? 'tsconfig.prod.json' : 'tsconfig.dev.json'
    },
    exclude: /node_modules/
  };
};
