// url loader used for embed content into bundle
module.exports.urlLoaderConfig = {
  test: /\/(assets)\/(embeded)\/([^\s]+)\.(png|jpeg|jpg|svg)$/i,
  use: [
    {
      loader: 'url-loader'
    }
  ]
};
