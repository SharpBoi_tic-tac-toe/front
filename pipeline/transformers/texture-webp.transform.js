const { isTexture } = require('../functions/is-texture');
const sharp = require('sharp');
const { getPathEnding } = require('../functions/get-path-ending');

module.exports.textureWebP = function (use = true) {
  return (
    /**
     * @param {Buffer} content
     * @param {string} from
     */
    async (content, from) => {
      if (!isTexture(from) || !use) {
        return content;
      }

      console.log('webp', getPathEnding(from));
      return await sharp(content).webp().toBuffer();
    }
  );
};
