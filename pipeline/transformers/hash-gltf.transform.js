const path = require('path');
const fs = require('fs');
const hasha = require('hasha');

const { isGLTF } = require('../functions/is-gltf');
const { getGltfImagesUri } = require('../functions/get-gltf-images-uri');

module.exports.hashGltf = function () {
  return (
    /**
     * @param {Buffer} content
     * @param {string} from
     */
    async (content, from) => {
      if (!isGLTF(from)) {
        return content;
      }

      console.log('gltf', from);

      const gltf = JSON.parse(content.toString());
      const dirname = path.dirname(from);
      const imagesUri = getGltfImagesUri(gltf);
      const imagesHashs = imagesUri.map(uri => {
        const imageBuffer = fs.readFileSync(path.resolve(dirname, uri));
        const imageExt = path.extname(uri);
        const imageHash = hasha(imageBuffer, { algorithm: 'md5' });
        return `${imageHash}${imageExt}`;
      });

      imagesUri.forEach((uri, index) => {
        setGltfImageUri(gltf, uri, imagesHashs[index]);
      });

      return Buffer.from(JSON.stringify(gltf));
    }
  );
};

/**
 *
 * @param {object} gltf
 * @param {string} searchUri
 * @param {string} newUri
 */
function setGltfImageUri(gltf, searchUri, newUri) {
  /** @type {({ uri: string })[]} */
  const images = gltf['images'];
  const imageManifest = images.find(manifest => manifest.uri === searchUri);
  imageManifest.uri = newUri;
}
