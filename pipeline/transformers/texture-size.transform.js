const sharp = require('sharp');
const { isTexture } = require('../functions/is-texture');
const { getPathEnding } = require('../functions/get-path-ending');

/**
 *
 * @param {number} maxSize
 */
module.exports.textureSize = function (maxSize) {
  return (
    /**
     * @param {Buffer} content
     * @param {string} from
     */
    async (content, from) => {
      if (!isTexture(from)) {
        return content;
      }

      const sharpTexture = sharp(content);
      const meta = await sharpTexture.metadata();

      if (isSizeOverMax(meta.width, meta.height, maxSize)) {
        const { width, height } = clampSizeToMax(meta.width, meta.height, maxSize);
        const resizedTexture = sharpTexture.resize(width, height);

        console.log(
          'transform texture',
          getPathEnding(from),
          `(${meta.width}; ${meta.height}) -> (${width}; ${height})`
        );

        return await resizedTexture.toBuffer();
      }

      return content;
    }
  );
};

function clampSizeToMax(sourceW, sourceH, max) {
  const aspectRatio = sourceW / sourceH;

  if (sourceW > sourceH) {
    return {
      width: max,
      height: max / aspectRatio
    };
  }

  return {
    width: max * aspectRatio,
    height: max
  };
}

function isSizeOverMax(w, h, max) {
  return w > max || h > max;
}
