const fs = require('fs');
const path = require('path');
const hasha = require('hasha');

const { isTexture } = require('../functions/is-texture');
const { isGLTF } = require('../functions/is-gltf');
const { isBin } = require('../functions/is-bin');

module.exports.hashGltfPaths = function () {
  return (
    /**
     * @param {string} targetPath
     * @param {string} absolutePath
     */
    async (targetPath, absolutePath) => {
      if (!(isTexture(absolutePath) || isGLTF(absolutePath) || isBin(absolutePath))) {
        return targetPath;
      }

      const buffer = fs.readFileSync(absolutePath);
      const ext = path.extname(absolutePath);
      const targetDir = path.dirname(targetPath);

      const bufferHash = hasha(buffer, { algorithm: 'md5' });

      return path.join(targetDir, `${bufferHash}${ext}`);
    }
  );
};
