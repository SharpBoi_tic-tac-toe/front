/**
 *
 * @param {((content: Buffer, from: string) => Promise<Buffer>)[]} transformers
 */
function chainTransforms(transformers) {
  return (
    /**
     * @param {Buffer} content
     * @param {string} from
     */
    async (content, from) => {
      let result = content;
      for (const transformer of transformers) {
        result = await transformer(result, from);
      }

      return result;
    }
  );
}

/**
 *
 * @param {((targetPath: string, absolutePath: string) => Promise<string>)[]} transformers
 * @returns {(targetPath: string, absolutePath: string) => Promise<string>}
 */
function chainPathTransforms(transformers) {
  return chainTransforms(transformers);
}

module.exports.chainTransforms = chainTransforms;
module.exports.chainPathTransforms = chainPathTransforms;
