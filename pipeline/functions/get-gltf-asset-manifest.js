const { isTexture } = require('./is-texture');
const { isGLTF } = require('./is-gltf');
const { isBin } = require('./is-bin');

const fs = require('fs');
const path = require('path');
const hasha = require('hasha');

/**
 *
 * @param {string} dstAllGltfsFolder
 * @param {string[]} gltfFolders
 * @returns {object}
 */
module.exports.getGltfAssetManifset = function (dstAllGltfsFolder, gltfFolders) {
  const foldersHashMaps = gltfFolders.map(gltfFolder => {
    const assetsHashMaps = getAssetsHashMaps(gltfFolder, dstAllGltfsFolder);

    return { ...assetsHashMaps };
  });

  return Object.assign({}, ...foldersHashMaps);
};

/**
 *
 * @param {string} srcGltfFolder
 * @param {string} dstAllGltfsFolder
 * @returns {object}
 */
function getAssetsHashMaps(srcGltfFolder, dstAllGltfsFolder) {
  const filesNames = fs.readdirSync(srcGltfFolder);
  const gltfFolderName = srcGltfFolder.split(path.sep).pop();

  const nameHashMaps = filesNames.map(fileName => {
    const fullFileName = path.resolve(srcGltfFolder, fileName);
    const hash = hasha.fromFileSync(fullFileName, { algorithm: 'md5' });
    const name = path.basename(fileName);
    const ext = path.extname(fileName);

    return {
      [name]: `${dstAllGltfsFolder}/${gltfFolderName}/${hash}${ext}`
    };
  });

  return Object.assign({}, ...nameHashMaps);
}
