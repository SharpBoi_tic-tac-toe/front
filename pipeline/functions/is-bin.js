const pattern = new RegExp(/\.bin$/);

module.exports.isBin = function (path) {
  return pattern.test(path);
};
