const pattern = new RegExp(/\.(png|jpg)$/i);

module.exports.isTexture = function (path) {
  return pattern.test(path);
};
