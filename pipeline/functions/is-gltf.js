const pattern = new RegExp(/\.gltf$/);

module.exports.isGLTF = function (path) {
  return pattern.test(path);
};
