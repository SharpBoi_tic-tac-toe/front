/**
 *
 * @param {string} path
 */
module.exports.getPathEnding = function (path) {
  const split = path.split('/');
  return `.../${split.slice(split.length - 2).join('/')}`;
};
