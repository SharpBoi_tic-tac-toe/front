/**
 *
 * @param {object} gltfJson
 */
module.exports.getGltfImagesUri = function (gltfJson) {
  /** @type {({ uri: string })[]} */
  const images = gltfJson['images'];

  return images.map(image => image.uri);
};
