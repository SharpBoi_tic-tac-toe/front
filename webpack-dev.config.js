const { webpackBaseConfig } = require('./webpack-base.config');
const { createTsxConfig } = require('./pipeline/configs/tsx.config');
const { chainTransforms, chainPathTransforms } = require('./pipeline/functions/chain-transform');
const { textureSize } = require('./pipeline/transformers/texture-size.transform');
const { hashGltfPaths } = require('./pipeline/transformers/hash-gltf-paths.transform');
const CopyPlugin = require('copy-webpack-plugin');
const { srcAssets3DPath, dstAssets3DPath } = require('./pipeline/consts/paths');

module.exports.webpackDevConfig = {
  ...webpackBaseConfig,
  module: {
    rules: [...webpackBaseConfig.module.rules, createTsxConfig('dev')]
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        {
          from: srcAssets3DPath,
          to: dstAssets3DPath,
          transform: chainTransforms([textureSize(1024)]),
          transformPath: chainPathTransforms([hashGltfPaths()])
        }
      ]
    }),
    ...webpackBaseConfig.plugins
  ],

  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist'
  }
};
