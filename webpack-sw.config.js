const path = require('path');
const webpack = require('webpack');
const fs = require('fs');
const { flatten } = require('array-flatten');
const TerserPlugin = require('terser-webpack-plugin');

const baseSWConfig = {
  entry: ['./service-worker/service-worker.ts'],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'service-worker.js'
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  plugins: [
    new webpack.DefinePlugin({
      $SW_HASH: webpack.DefinePlugin.runtimeValue(createSWHash, getFilesRecursively('src'))
    })
  ]
};

const devSWConfig = {
  ...baseSWConfig,

  module: {
    rules: [
      {
        test: /\.ts(x)?$/,
        loader: 'ts-loader',
        options: {
          configFile: 'service-worker/tsconfig.dev.json'
        },
        exclude: /node_modules/
      }
    ]
  },

  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist'
  }
};
const prodSWConfig = {
  ...baseSWConfig,

  module: {
    rules: [
      {
        test: /\.ts(x)?$/,
        loader: 'ts-loader',
        options: {
          configFile: 'service-worker/tsconfig.prod.json'
        },
        exclude: /node_modules/
      }
    ]
  },

  mode: 'production',
  devtool: 'none',
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        extractComments: false
      })
    ]
  }
};

module.exports.webpackServiceWorkerConfig = (isProd = false) => {
  if (isProd) return prodSWConfig;

  return devSWConfig;
};

function createSWHash() {
  const hash = Date.now();
  console.log('SW HASH GEN', hash);
  return JSON.stringify(hash);
}

/**
 *
 * @param {string} from
 * @param {true | boolean} outFullPath
 * @returns {string[]}
 */
function getFilesRecursively(from, outFullPath = true) {
  const dirEntity = fs.readdirSync(from).map(entity => path.join(from, entity));
  const dirFiles = dirEntity.filter(entity => fs.statSync(entity).isFile());
  const dirFolders = dirEntity.filter(entity => fs.statSync(entity).isDirectory());

  const deeperFiles = dirFolders.map(folder => getFilesRecursively(folder));
  const result = [...dirFiles, ...flatten(deeperFiles)];

  if (!outFullPath) {
    return result;
  }

  return result.map(file => path.resolve(__dirname, file));
}
