const { webpackDevConfig } = require('./webpack-dev.config');
const { webpackProdConfig } = require('./webpack-prod.config');
const { webpackServiceWorkerConfig } = require('./webpack-sw.config');

module.exports = env => {
  const isDev = env.MODE === 'development';
  const isProd = env.MODE === 'production';

  if (isDev) return [webpackDevConfig, webpackServiceWorkerConfig(isProd)];
  if (isProd) return [webpackProdConfig, webpackServiceWorkerConfig(isProd)];
}
