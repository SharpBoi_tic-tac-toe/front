declare module '*.scss' {
  const content: { [key: string]: string };
  export default content;
}

declare module '*.module.css' {
  const content: { [key: string]: string };
  export default content;
}

declare module '*.jpg' {
  const url: string;
  export default url;
}
declare module '*.svg' {
  const url: string;
  export default url;
}

declare interface Window {
  /**
   * @deprecated
   */
  devToolsExtension: (...args: any) => any;
  __REDUX_DEVTOOLS_EXTENSION__: () => any;
}
