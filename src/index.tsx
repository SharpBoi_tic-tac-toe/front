import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import { appHistory } from './app/constants/history.const';
import { getInject } from './app/constants/injectors.const';
import { App } from './app/containers/app/App';
import { syncRouteState } from './app/functions/sync-history-state';
import { LocalStorageService } from './app/services/local-storage.service';
import { SWManagerService } from './app/services/sw-manager.service';
import { appSettingsStorageKey, initialAppSettingsStorage } from './app/storage/app-settings.storage';
import { appStore } from './app/store/app-store';
import './index.scss';
import './styles/fonts.scss';

const swManager = getInject(SWManagerService);
void swManager.register();
swManager.onSWFirstInstall$.subscribe(() => {
  window.location.reload();
});

const localStorageService = getInject(LocalStorageService);
localStorageService.initializeValue(appSettingsStorageKey, initialAppSettingsStorage);

const appRoot: HTMLElement = document.getElementById('app-root');

ReactDOM.render(
  <Provider store={appStore}>
    <Router history={syncRouteState(appHistory, appStore)}>
      <App />
    </Router>
  </Provider>,
  appRoot
);
