export type SWRegisterResult = 'not_supported' | 'success' | 'error' | 'pending';
