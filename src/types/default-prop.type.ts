import React from 'react';

export type DefaultProp<T> = {
  [K in keyof T]: T[K];
} & { children?: React.ReactNode };
