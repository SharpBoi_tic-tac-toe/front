import { PlayerType } from './player.type';

export type WinnerType = PlayerType | 'draw' | 'unknown';
