import { PlayerType } from './player.type';

export type CellSetBy = PlayerType | 'free';
