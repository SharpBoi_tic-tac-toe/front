import { PlayerType } from './player.type';

export type NextTurnBy = PlayerType | 'unknown';
