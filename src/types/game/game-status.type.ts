export type GameStatus = 'not_created' | 'loadingContent' | 'process' | 'end';
