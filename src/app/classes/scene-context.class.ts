import { AnimationClip, Group, PerspectiveCamera, Scene } from 'three';
import { GLTF } from '../../reexports/gltf-loader';
import { ObjectsTags } from '../enums/objects-tags';
import { hasField } from '../functions/has.function';
import { disposeScene } from '../functions/three/dispose-scene.function';
import { IDisposable } from '../interfaces/disposable.interface';

export class SceneContext implements IDisposable {
  public readonly scene: Scene;
  public readonly mainCamera: PerspectiveCamera;
  public readonly animations: AnimationClip[];

  constructor(gltf: GLTF, public readonly sceneUrl: string) {
    this.scene = this.parseGroupToScene(gltf.scene);
    this.mainCamera = this.getMainCamera(gltf);
    this.animations = gltf.animations;
  }

  public dispose(): void {
    disposeScene(this.scene);
    this.animations.splice(0);
  }

  private parseGroupToScene(sceneGroup: Group): Scene {
    const scene: Scene = new Scene();
    scene.add(...sceneGroup.children);
    return scene;
  }
  private getMainCamera(gltf: GLTF): PerspectiveCamera {
    const camera = gltf.cameras.find(gltfCamera => hasField(gltfCamera.parent.userData, ObjectsTags.MainCamera));

    if (camera instanceof PerspectiveCamera) {
      return camera;
    }

    return null;
  }
}
