import { Subject, Subscription } from 'rxjs';
import {
  Clock,
  PCFSoftShadowMap,
  sRGBEncoding,
  WebGLRenderer,
  WebGLRendererParameters
} from 'three';
import { gammaFactor } from '../constants/graphics.const';
import { isNil } from '../functions/isNil.function';
import { IDisposable } from '../interfaces/disposable.interface';
import { SceneContext } from './scene-context.class';

type UpdateCallback = (deltaTime?: number) => void;

export class RenderGenerator implements IDisposable {
  public readonly canvas: HTMLCanvasElement;

  private readonly updateEvent$: Subject<number> = new Subject();
  private readonly updateSubscriptions: Subscription = new Subscription();
  private readonly renderer: WebGLRenderer;
  private readonly clock: Clock = new Clock(false);
  private readonly pixelRatio: number = 1;

  private sceneUrlValue: string = '';
  private isRendering: boolean = false;

  private canvasWidthValue: number = 0;
  private canvasHeightValue: number = 0;

  public get sceneUrl(): string {
    return this.sceneUrlValue;
  }
  public get canvasWidth(): number {
    return this.canvasWidthValue;
  }
  public get canvasHeight(): number {
    return this.canvasHeightValue;
  }

  constructor(
    private sceneContext: SceneContext,
    canvas: HTMLCanvasElement,
    renderParams?: WebGLRendererParameters
  ) {
    this.renderer = new WebGLRenderer({
      ...renderParams,

      canvas,
      context: canvas.getContext('webgl')
    });
    this.renderer.setPixelRatio(this.pixelRatio);
    this.renderer.outputEncoding = sRGBEncoding;
    this.renderer.gammaFactor = gammaFactor;

    this.setSceneContext(sceneContext);
    this.canvas = canvas;
  }

  public startRenderLoop(): void {
    if (this.isRendering) {
      return;
    }

    this.isRendering = true;
    this.clock.start();
    this.renderer.setAnimationLoop(this.renderLoop.bind(this));
  }
  public stopRenderLoop(): void {
    this.isRendering = false;
    this.renderer.setAnimationLoop(null);
    this.clock.stop();
  }

  public dispose(): void {
    this.updateSubscriptions.unsubscribe();
    this.stopRenderLoop();
    this.renderer.dispose();
  }

  public addUpdate(updateCallback: UpdateCallback): Subscription {
    const currentUpdateSubscription: Subscription = this.updateEvent$.subscribe(updateCallback);
    this.updateSubscriptions.add(currentUpdateSubscription);
    return currentUpdateSubscription;
  }

  public setSceneContext(sceneCtx: SceneContext): void {
    if (isNil(sceneCtx)) {
      this.sceneContext = null;
      this.sceneUrlValue = '';
      return;
    }

    this.sceneContext = sceneCtx;
    this.sceneUrlValue = sceneCtx.sceneUrl;
  }

  public setShadow(enabled: boolean): void {
    const { shadowMap } = this.renderer;

    shadowMap.enabled = enabled;
    shadowMap.autoUpdate = true;
    shadowMap.type = PCFSoftShadowMap;
  }

  private renderLoop(): void {
    this.updateCanvasSize();
    this.updateEvent$.next(this.clock.getDelta());
    this.renderer.render(this.sceneContext.scene, this.sceneContext.mainCamera);
  }

  private updateCanvasSize(): void {
    const rect = this.canvas.getBoundingClientRect();
    this.canvasWidthValue = rect.width;
    this.canvasHeightValue = rect.height;
  }
}
