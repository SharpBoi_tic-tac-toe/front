import { Object3D } from 'three';
import { hasField } from '../functions/has.function';
import { isNil } from '../functions/isNil.function';
import { traverseChildren } from '../functions/three/traverse-children.function';
import { IDisposable } from '../interfaces/disposable.interface';

export class PrefabComposer implements IDisposable {
  private readonly treeElements: Object3D[] = [];
  private readonly assets: Map<string, Object3D> = new Map();

  constructor(root: Object3D, prefabsTags: string[]) {
    this.treeElements = traverseChildren(root);

    this.fillPrefabs(prefabsTags);
  }

  public getPrefab(tag: string): Object3D {
    return this.assets.get(tag);
  }
  public getAllPrefabs(): Object3D[] {
    return Array.from(this.assets.values());
  }
  public instancePrefab(tag: string): Object3D {
    return this.getPrefab(tag).clone(true);
  }


  public dispose(): void {
    this.treeElements.splice(0);
    this.assets.clear();
  }


  private fillPrefabs(assetsTags: string[]): void {
    assetsTags.forEach(tag => {
      const asset: Object3D = this.treeElements.find(element => hasField(element.userData, tag));

      if (isNil(asset)) {
        throw new Error(`asset not found '${tag}'`);
      }

      this.assets.set(tag, asset);
      asset.parent.remove(asset);
    });
  }
}
