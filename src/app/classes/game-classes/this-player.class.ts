import { Observable, Subject, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Vector2 } from 'three';
import { inject } from '../../constants/injectors.const';
import { filterIsTrue } from '../../functions/rx/filter-is-true.function';
import { filterNotNil } from '../../functions/rx/filter-not-nil.function';
import { IDisposable } from '../../interfaces/disposable.interface';
import { IPlayer } from '../../interfaces/game/player.interface';
import { CellDetectorService } from '../../services/game/cell-detector.service';
import { appStore$ } from '../../store/app-store';
import { IGameState } from '../../store/game/state';

export class ThisPlayer implements IPlayer, IDisposable {
  @inject(CellDetectorService) private readonly cellDetector: CellDetectorService;

  public readonly onSetCell$: Subject<Vector2> = new Subject();

  private readonly gameState$: Observable<IGameState> = appStore$.pipe(map(state => state.game));
  private readonly thisPlayerNextTurn$ = this.gameState$.pipe(map(state => state.nextTurnBy === 'self'));
  private readonly subscription: Subscription = new Subscription();

  constructor() {
    this.subscription.add(this.handleClickSetCell());
  }

  public dispose(): void {
    this.subscription.unsubscribe();
    this.onSetCell$.complete();
  }

  private handleClickSetCell(): Subscription {
    return this.cellDetector.clickedCellIndex$
      .pipe(filterNotNil())
      .subscribe(cellIndex => {
        this.thisPlayerNextTurn$.pipe(take(1), filterIsTrue()).subscribe(() => {
          this.onSetCell$.next(cellIndex);
        });
      });
  }
}
