import { AnimationAction, AnimationClip, AnimationMixer, Object3D } from 'three';
import { IDisposable } from '../../interfaces/disposable.interface';
import { SceneContext } from '../scene-context.class';

export class AnimationController implements IDisposable {
  private readonly clips: Set<AnimationClip> = new Set();
  private readonly mainAnimator: AnimationMixer;

  private readonly individualAnimators: AnimationMixer[] = [];

  constructor(
    private readonly sceneInstance: SceneContext
  ) {
    this.mainAnimator = new AnimationMixer(sceneInstance.scene);
  }

  public dispose(): void {
    this.clips.forEach(clip => {
      this.mainAnimator.uncacheAction(clip);
      this.mainAnimator.uncacheClip(clip);
    });
    this.mainAnimator.uncacheRoot(this.sceneInstance.scene);

    this.individualAnimators.forEach(animator => {
      this.clips.forEach(clip => {
        animator.uncacheClip(clip);
        animator.uncacheAction(clip);
      });
      animator.uncacheRoot(animator.getRoot());
    });
  }
  public update(deltaTime: number): void {
    this.mainAnimator.update(deltaTime);

    this.individualAnimators.forEach(animator => {
      animator.update(deltaTime);
    });
  }

  public getAnimationClip(clipName: string): AnimationClip {
    const clip: AnimationClip = this.sceneInstance.animations
      .find(animation => animation.name === clipName);
    this.clips.add(clip);
    return clip;
  }
  public createLinkedAnimation(clipName: string): AnimationAction {
    return this.mainAnimator.clipAction(this.getAnimationClip(clipName));
  }

  public createIndividualAnimation(root: Object3D, clip: AnimationClip): AnimationAction {
    const animator = new AnimationMixer(root);
    this.individualAnimators.push(animator);
    return animator.clipAction(clip);
  }
}
