import { Subscription } from 'rxjs';
import { AmbientLight, Light, Object3D } from 'three';
import { shadowBias, shadowMapSize } from '../../constants/graphics.const';
import { forceImportInjectable, inject } from '../../constants/injectors.const';
import { ObjectsTags } from '../../enums/objects-tags';
import { hasField } from '../../functions/has.function';
import { traverseChildren, traverseType } from '../../functions/three/traverse-children.function';
import { IPlayer } from '../../interfaces/game/player.interface';
import { CellDetectorService } from '../../services/game/cell-detector.service';
import { CellSetterService } from '../../services/game/cell-setter.service';
import { CursorModeService } from '../../services/game/cursor-mode.service';
import { FigurePlacerService } from '../../services/game/figure-placer.service';
import { FigurePlacingAnimatorService } from '../../services/game/figure-placing-animator.service';
import { TableAnimatorService } from '../../services/game/table-animator.service';
import { BasicGameContext } from '../basic-game-context.class';
import { PrefabComposer } from '../prefab-composer.class';
import { RenderGenerator } from '../render-generator.class';
import { SceneContext } from '../scene-context.class';
import { AIPlayer } from './ai-player.class';
import { GameFieldModel } from './game-field-model.class';
import { GameProcessor } from './game-processor.class';
import { ThisPlayer } from './this-player.class';

forceImportInjectable(CursorModeService);

export class MainGameContext extends BasicGameContext {
  @inject(CellDetectorService) private readonly cellDetector: CellDetectorService;
  @inject(FigurePlacerService) private readonly figurePlacer: FigurePlacerService;
  @inject(CellSetterService) private readonly cellSetter: CellSetterService;
  @inject(FigurePlacingAnimatorService)
  private readonly figurePlacingAnimator: FigurePlacingAnimatorService;
  @inject(TableAnimatorService) private readonly tableAnimator: TableAnimatorService;

  // private readonly gameState$ = appStore$.pipe(map(state => state.game));

  private readonly prefabComposer: PrefabComposer;

  private readonly subscription: Subscription = new Subscription();

  private readonly ambientLight: AmbientLight = new AmbientLight();
  private readonly tablePivot: Object3D;
  private readonly tableModel: GameFieldModel;

  private readonly thisPlayer: IPlayer;
  private readonly opponentPlayer: IPlayer;
  private readonly gameProcessor: GameProcessor;

  constructor(sceneInstance: SceneContext, rendererInstance: RenderGenerator) {
    super(sceneInstance, rendererInstance);
    const { mainCamera } = sceneInstance;

    this.setupGraphics();

    this.prefabComposer = this.createPrefabs();
    this.tablePivot = this.getTablePivot();
    this.tableModel = new GameFieldModel(this.tablePivot);

    this.cellDetector.setSceneEnvironment(this.tableModel, this.inputProvider, mainCamera);
    this.tableAnimator.setSceneEnvironment(
      this.tablePivot,
      this.inputProvider,
      this.animationController
    );
    this.figurePlacer.setSceneEnvironment(this.prefabComposer, this.tableModel);
    this.figurePlacingAnimator.setSceneEnvironment(this.animationController);

    this.thisPlayer = this.createThisPlayer();
    this.opponentPlayer = this.createOpponentPlayer();
    this.gameProcessor = new GameProcessor();
  }

  public dispose(): void {
    this.subscription.unsubscribe();

    this.figurePlacingAnimator.dispose();
    this.gameProcessor.dispose();
    this.opponentPlayer.dispose();
    this.thisPlayer.dispose();

    super.dispose();
  }

  protected update(deltaTime: number): void {
    this.tableAnimator.update(deltaTime);
  }
  private getTablePivot(): Object3D {
    return traverseChildren(this.sceneInstance.scene).find(child =>
      hasField(child.userData, ObjectsTags.TablePivot)
    );
  }

  private createThisPlayer(): IPlayer {
    const player = new ThisPlayer();
    this.subscription.add(
      player.onSetCell$.subscribe(cellIndex => {
        this.cellSetter.setCell(cellIndex, 'self');
      })
    );
    return player;
  }
  private createOpponentPlayer(): IPlayer {
    const player = new AIPlayer();
    this.subscription.add(
      player.onSetCell$.subscribe(cellIndex => {
        this.cellSetter.setCell(cellIndex, 'opponent');
      })
    );
    return player;
  }
  private createPrefabs(): PrefabComposer {
    return new PrefabComposer(this.sceneInstance.scene, [ObjectsTags.FigureO, ObjectsTags.FigureX]);
  }

  private setupGraphics(): void {
    this.rendererInstance.setShadow(true);
    const { scene } = this.sceneInstance;

    traverseChildren(scene).forEach(child => {
      child.castShadow = true;
      child.receiveShadow = true;
    });
    traverseType(scene, Light).forEach(light => {
      const { shadow } = light;
      shadow.mapSize.set(shadowMapSize, shadowMapSize);
      shadow.bias = shadowBias;
    });

    scene.add(this.ambientLight);
    this.ambientLight.intensity = 0.25;
  }
}
