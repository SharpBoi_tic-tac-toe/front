import { Observable, Subject, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Vector2 } from 'three';
import { rnd } from '../../functions/random.function';
import { filterNotNil } from '../../functions/rx/filter-not-nil.function';
import { IDisposable } from '../../interfaces/disposable.interface';
import { CellData } from '../../interfaces/game/cell-data.interface';
import { IPlayer } from '../../interfaces/game/player.interface';
import { appStore$ } from '../../store/app-store';
import { IGameState } from '../../store/game/state';

export class AIPlayer implements IPlayer, IDisposable {
  public readonly onSetCell$: Subject<Vector2> = new Subject();

  private readonly maxThinkDelay: number = 1500;
  private readonly minThinkDelay: number = 500;

  private readonly subscription: Subscription = new Subscription();
  private readonly gameState$: Observable<IGameState> = appStore$.pipe(map(state => state.game));

  constructor() {
    this.subscription.add(this.handleSyntheticPlayerActions());
  }
  public dispose(): void {
    this.subscription.unsubscribe();
    this.onSetCell$.complete();
  }

  private handleSyntheticPlayerActions(): Subscription {
    return this.gameState$
      .pipe(
        filter(state => state.status === 'process'),
        filter(state => state.nextTurnBy === 'opponent'),
        map(state => this.getRandomFreeCell(state.cells)),
        filterNotNil()
      )
      .subscribe(randomFreeCell => {
        setTimeout(() => {
          this.onSetCell$.next(randomFreeCell.index);
        }, rnd(this.minThinkDelay, this.maxThinkDelay));
      });
  }

  private getRandomFreeCell(cells: CellData[]): CellData {
    const freeCells = cells.filter(cell => cell.setBy === 'free');
    const randomIndex = Math.floor(rnd(freeCells.length));
    return freeCells[randomIndex];
  }
}
