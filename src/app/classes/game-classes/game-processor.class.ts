import { Observable, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { TTTGameProcessor, TTTTurnResult } from '../../../../../shared';
import { CellSetBy } from '../../../types/game/cell-set-by.type';
import { WinnerType } from '../../../types/game/winner.type';
import { IDisposable } from '../../interfaces/disposable.interface';
import { CellData } from '../../interfaces/game/cell-data.interface';
import { appStore, appStore$ } from '../../store/app-store';
import { setGameStatus, setGameWinner } from '../../store/game/actions';
import { IGameState } from '../../store/game/state';

export class GameProcessor implements IDisposable {
  private readonly gameState$: Observable<IGameState> = appStore$.pipe(map(state => state.game));
  private readonly subscription: Subscription = new Subscription();

  constructor() {
    this.subscription.add(this.handleGameProcessing());
  }

  public dispose(): void {
    this.subscription.unsubscribe();
  }

  private handleGameProcessing(): Subscription {
    const processor = new TTTGameProcessor({
      fieldSize: 3,
      winStreak: 3
    });

    return this.gameState$
      .pipe(
        filter(state => state.status === 'process'),
        filter(state => state.winner === 'unknown'),
        map(state => this.getLastCell(state.cells)),
        filter(lastCell => lastCell.setBy !== 'free')
      )
      .subscribe(lastCell => {
        const { index, setBy } = lastCell;
        const turnData = processor.tryMakeTurn(index.x, index.y, lastCell.setBy);
        const { turnResult } = turnData;

        if (turnResult === TTTTurnResult.Win || turnResult === TTTTurnResult.Draw) {
          const winner = this.getWinner(setBy, turnResult);

          appStore.dispatch(setGameWinner(winner));
          appStore.dispatch(setGameStatus('end'));
        }
      });
  }

  private getLastCell(cells: CellData[]): CellData {
    const sortedCells = [...cells].sort((a, b) => a.changeOrder - b.changeOrder);
    return sortedCells.pop();
  }
  private getWinner(setBy: CellSetBy, turnResult: TTTTurnResult): WinnerType {
    if (turnResult === TTTTurnResult.Draw) {
      return 'draw';
    }

    if (setBy === 'self') {
      return 'self';
    }

    if (setBy === 'opponent') {
      return 'opponent';
    }
  }
}
