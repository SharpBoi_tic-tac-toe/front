import { Object3D, Vector2 } from 'three';
import { ObjectsTags } from '../../enums/objects-tags';
import { hasField } from '../../functions/has.function';
import { traverseChildren } from '../../functions/three/traverse-children.function';

export class GameFieldModel {
  public readonly cells: Object3D[] = [];
  public readonly cellsIndeces2D: Vector2[] = [];

  constructor(
    private readonly tableRoot: Object3D
  ) {
    this.parseCells();
  }

  public getCellByIndex(index: Vector2): Object3D {
    const cellIndex = this.cellsIndeces2D.findIndex(index2d => index2d.equals(index));
    return this.cells[cellIndex];
  }
  public getIndexByCell(cell: Object3D): Vector2 {
    const index2DIndex = this.cells.findIndex(tableCell => tableCell === cell);
    return this.cellsIndeces2D[index2DIndex];
  }

  private parseCells(): void {
    const children = traverseChildren(this.tableRoot);
    children.forEach(child => {
      if (!hasField(child.userData, ObjectsTags.Cell)) {
        return;
      }

      const cellIndexString: string = child.userData[ObjectsTags.Cell];
      const cellIndex: number[] = cellIndexString.split(' ').map(index => Number(index));

      this.cells.push(child);
      this.cellsIndeces2D.push(new Vector2(cellIndex[0], cellIndex[1]));
    });
  }
}
