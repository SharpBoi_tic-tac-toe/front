import { Subscription } from 'rxjs';
import { ICanvasInput } from '../interfaces/canvas-input.interface';
import { IDisposable } from '../interfaces/disposable.interface';
import { DesktopCanvasInput } from './desktop-canvas-input.class';
import { AnimationController } from './game-classes/animation-controller.class';
import { RenderGenerator } from './render-generator.class';
import { SceneContext } from './scene-context.class';

export abstract class BasicGameContext implements IDisposable {
  protected readonly inputProvider: ICanvasInput;
  protected animationController: AnimationController;

  private readonly privateSubscription: Subscription = new Subscription();

  constructor(protected sceneInstance: SceneContext, protected rendererInstance: RenderGenerator) {
    this.inputProvider = this.createInputProvider();
    this.animationController = new AnimationController(sceneInstance);

    this.createUpdate();
  }
  public dispose(): void {
    this.inputProvider.dispose();
    this.privateSubscription.unsubscribe();
    this.animationController.dispose();
  }

  private createUpdate(): void {
    this.privateSubscription
      .add(this.rendererInstance.addUpdate(this.update.bind(this)))
      .add(this.rendererInstance.addUpdate(this.internalUpdate.bind(this)));
  }
  private internalUpdate(deltaTime: number): void {
    this.animationController.update(deltaTime);
  }
  private createInputProvider(): ICanvasInput {
    return new DesktopCanvasInput(this.rendererInstance);
  }

  protected abstract update(deltaTime: number): void;
}
