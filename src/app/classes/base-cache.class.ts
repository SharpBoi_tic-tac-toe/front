import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export class BaseCache<TcacheElement> {
  protected readonly cache$: BehaviorSubject<Map<string, TcacheElement>> = new BehaviorSubject(new Map());

  public setElement(id: string, element: TcacheElement): void {
    const cache = this.cache$.getValue();

    if (cache.has(id)) {
      throw new Error(`cache element with id ${id} already exists`);
    }

    cache.set(id, element);
    this.cache$.next(cache);
  }

  public getElement(id: string): TcacheElement {
    return this.cache$.getValue().get(id);
  }
  public getElement$(id: string): Observable<TcacheElement | undefined> {
    return this.cache$.pipe(
      map(cache => cache.get(id))
    );
  }

  public getCache$(): BehaviorSubject<Map<string, TcacheElement>> {
    return this.cache$;
  }
  public hasElement$(id: string): Observable<boolean> {
    return this.cache$.pipe(
      map(cache => cache.has(id))
    );
  }
  public removeElement(id: string): void {
    const cache = this.cache$.getValue();
    cache.delete(id);
    this.cache$.next(cache);
  }
}
