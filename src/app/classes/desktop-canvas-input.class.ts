import { BehaviorSubject, Subject } from 'rxjs';
import { Vector2 } from 'three';
import { AnyFunction } from '../../types/any-function.type';
import { clipCoordinates } from '../functions/three/clip-coordinates.function';
import { ICanvasInput } from '../interfaces/canvas-input.interface';
import { IDisposable } from '../interfaces/disposable.interface';
import { RenderGenerator } from './render-generator.class';

export class DesktopCanvasInput implements ICanvasInput, IDisposable {
  public readonly onPointerDown: BehaviorSubject<Vector2> = new BehaviorSubject(null);
  public readonly onPointerMove: BehaviorSubject<Vector2> = new BehaviorSubject(null);
  public readonly onPointerUp: BehaviorSubject<Vector2> = new BehaviorSubject(null);
  public readonly onPointerLeave: Subject<void> = new Subject();
  public readonly isPointerOut: Subject<boolean> = new Subject();

  private readonly eventsHandlers: Map<string, AnyFunction> = new Map();

  constructor(private readonly renderCtx: RenderGenerator) {
    this.handleCanvasEvents();
  }
  public dispose(): void {
    this.disposeAllHandlers();

    this.onPointerDown.complete();
    this.onPointerMove.complete();
    this.onPointerUp.complete();
  }

  private handleCanvasEvents(): void {
    this.addHandler('mousedown', this.handlePointerDown);
    this.addHandler('mousemove', this.handlePointerMove);
    this.addHandler('mouseup', this.handlePointerUp);
    this.addHandler('mouseenter', this.handlePointerEnter);
    this.addHandler('mouseleave', this.handlePointerLeave);
  }
  private readonly handlePointerDown = (canvasEvent: MouseEvent): void => {
    this.onPointerDown.next(this.getCanvasClipCoords(canvasEvent));
  };
  private readonly handlePointerMove = (canvasEvent: MouseEvent): void => {
    this.onPointerMove.next(this.getCanvasClipCoords(canvasEvent));
  };
  private readonly handlePointerUp = (canvasEvent: MouseEvent): void => {
    this.onPointerUp.next(this.getCanvasClipCoords(canvasEvent));
  };
  private readonly handlePointerEnter = (): void => {
    this.isPointerOut.next(false);
  };
  private readonly handlePointerLeave = (): void => {
    this.isPointerOut.next(true);
    this.onPointerLeave.next();
  };

  private addHandler<K extends keyof HTMLElementEventMap>(key: K, handler: AnyFunction): void {
    const { canvas } = this.renderCtx;

    canvas.addEventListener(key, handler);
    this.eventsHandlers.set(key, handler);
  }
  private disposeAllHandlers(): void {
    const { canvas } = this.renderCtx;

    this.eventsHandlers.forEach((handler, key) => {
      canvas.removeEventListener(key, handler);
    });
    this.eventsHandlers.clear();
  }

  private getCanvasLocalCoords(canvasEvent: MouseEvent): Vector2 {
    return new Vector2(canvasEvent.offsetX, canvasEvent.offsetY);
  }
  private getCanvasClipCoords(canvasEvent: MouseEvent): Vector2 {
    const { canvasWidth, canvasHeight } = this.renderCtx;

    const localCoords = this.getCanvasLocalCoords(canvasEvent);
    return clipCoordinates(localCoords, canvasWidth, canvasHeight);
  }
}
