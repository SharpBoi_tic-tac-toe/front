import { Action, Dispatch, Middleware, MiddlewareAPI } from 'redux';
import { CreatedAction } from './create-action';

type MiddlewareCallback<Tstate, Taction extends Action> =
  (api: MiddlewareAPI<Dispatch, Tstate>, next: Dispatch<Taction>, action: Taction) => Taction;

export function createMiddleware<Tstate, Taction extends Action = CreatedAction<unknown>>(middleware: MiddlewareCallback<Tstate, Taction>): Middleware {
  return (
    (api: MiddlewareAPI<Dispatch, Tstate>) =>
      (next: Dispatch<Taction>) =>
        (action: Taction) =>
          middleware(api, next, action));
}
