import { ActionCreator, CreatedAction } from './create-action';
import { Reducer, AnyAction } from 'redux';
import { isNil } from '../functions/isNil.function';

type ActionHandler<Tstate> = <Tpayload>(actionCreator: ActionCreator<Tpayload>, callback: ActionCallback<Tstate, Tpayload>) => ReducerContext<Tstate>;
type ActionCallback<Tstate, Tpayload> = (state: Tstate, action: CreatedAction<Tpayload>) => Tstate;
type AnyActionCallback<Tstate> = ActionCallback<Tstate, any>;
export interface ReducerContext<Tstate> {
  handleAction: ActionHandler<Tstate>;
  mixHandlers: () => Reducer<Tstate>;
}

export function createReducer<Tstate>(initialState: Tstate): ReducerContext<Tstate> {
  const handlers: Map<string, AnyActionCallback<Tstate>> = new Map();

  const context: ReducerContext<Tstate> = {
    handleAction<Tpayload = unknown>(actionCreator: ActionCreator<Tpayload>, callback: ActionCallback<Tstate, Tpayload>) {
      handlers.set(actionCreator.type, callback);
      return context;
    },

    mixHandlers() {
      return (state: Tstate = initialState, action: AnyAction) => {
        const handler: AnyActionCallback<Tstate> = handlers.get(action.type);
        return isNil(handler) ? state : handler(state, action as CreatedAction<unknown>);
      };
    }
  };

  return context;
}
