import { Action } from 'redux';
import { AnyFunction } from '../../types/any-function.type';

export type ActionResult<TactionCreator extends AnyFunction> = ReturnType<TactionCreator>;
export interface CreatedAction<Tpayload = void> extends Action<string> {
  payload: Tpayload;
}
export interface ActionCreator<Tpayload = void> {
  type: string;
  (payload: Tpayload): CreatedAction<Tpayload>;
}

export function createAction<Tpayload = void>(type: string): ActionCreator<Tpayload> {
  const creator: ActionCreator<Tpayload> = (payload: Tpayload) => ({
    type,
    payload
  });
  creator.type = type;
  return creator;
}
