export function actionType(sectionName: string, actionTypeName: string): string {
  return `@@${sectionName}/${actionTypeName}`;
}
