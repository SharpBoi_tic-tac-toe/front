import React from 'react';

import style from './header.module.scss';
import arrowSrc from '../../../assets/static/images/arrow.svg';

interface IHeaderProps {
  title?: string;
  onClickBackArrow?: () => void;
}

export const Header: React.FC<IHeaderProps> = (props: IHeaderProps) =>
  <div className={style.header}>
    <div className={style.arrow_wrapper}>
      <img className={style.arrow} src={arrowSrc} onClick={props.onClickBackArrow}/>
    </div>
    <span className={style.title}>{props.title}</span>
  </div>
;
