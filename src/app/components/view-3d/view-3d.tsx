import React, { MutableRefObject, useEffect, useRef } from 'react';
import { getInject } from '../../constants/injectors.const';
import { CanvasCacheService } from '../../services/canvas-cache.service';
import style from './view-3d.module.scss';

const canvasSize: number = 500;

interface Props {
  canvasId: string;
  isLoaded?: boolean;
}
export const View3D: React.FC<Props> = props => {
  const { canvasId, isLoaded } = props;
  const canvasRef: MutableRefObject<HTMLCanvasElement> = useRef();
  const canvasCache: CanvasCacheService = getInject(CanvasCacheService);

  useEffect(() => {
    canvasCache.setElement(canvasId, canvasRef.current);

    return () => {
      canvasCache.removeElement(canvasId);
    };
  }, []);

  return (
    <div className={style.view_3d}>
      <div className={style.canvas_wrapper}>
        <canvas
          width={canvasSize}
          height={canvasSize}
          ref={canvasRef}
          className={`${isLoaded && style.loaded}`}
        />
      </div>

      <div className={`${style.loading_overlay} ${isLoaded && style.loaded}`}>
        <span className={style.loading_title}>{'Loading 3D...'}</span>
      </div>
    </div>
  );
};
