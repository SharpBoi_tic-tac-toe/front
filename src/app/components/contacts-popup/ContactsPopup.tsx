import React, { FC, useCallback, useState } from 'react';
import style from './contacts.module.scss';

export const ContactsPopup: FC = () => {
  const [showContacts, setShowContacts] = useState(false);

  const handleClick = useCallback(() => {
    setShowContacts(value => !value);
  }, [setShowContacts]);

  return (
    <div className={style.icon} >
      <span
        onClick={handleClick}
        className={style.contacts_button}>
        {'Contacts'}
      </span>
      {showContacts &&
      <div className={style.contacts_popup}>
        <div>{'Hello :)'}</div>
        <div>{'If you like this project or you noticed a bug, feel free to messege me !'}</div>
        <br/>
        <div>{'Telegram: @SharpBoi'}</div>
        <div>{'Mail: dkapustin1998@gmail.com'}</div>
        <div>{'Instagram: @frobeg69'}</div>
      </div>}

    </div>
  );
};
