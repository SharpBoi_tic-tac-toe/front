import React from 'react';
import { DefaultProp } from '../../../../types/default-prop.type';
import style from './menu-item.module.scss';


interface IMenuItemProps {
  onClick?: () => void;
  disable?: boolean;
}

export const MenuItem: React.FC<IMenuItemProps> = (props: DefaultProp<IMenuItemProps>) => {
  const { disable } = props;

  return (
    <div
      className={`${style.item_row} ${disable && style.disabled}`}
      onClick={props.onClick}
    >
      <span className={style.content}>{props.children}</span>
    </div>
  );
};
