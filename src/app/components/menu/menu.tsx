import React from 'react';
import { DefaultProp } from '../../../types/default-prop.type';

import style from './menu.module.scss';

interface IMenuProps {
  title?: string;
  footer?: string;
}

export const Menu: React.FC<IMenuProps> = (props: DefaultProp<IMenuProps>) =>
  <div className={style.container}>
    <div className={style.title_wrapper}>
      <span>{props.title}</span>
    </div>

    <div className={style.contents}>{props.children}</div>

    <div className={style.footer_wrapper}>
      <span>{props.footer}</span>
    </div>
  </div>
;
