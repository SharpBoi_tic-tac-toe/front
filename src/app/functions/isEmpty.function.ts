import { isArray } from './isArray.function';

export function isEmpty<T>(value: T[]): boolean {
  if (!isArray(value)) {
    throw new Error('not array passed to check length');
  }

  return value.length === 0;
}
