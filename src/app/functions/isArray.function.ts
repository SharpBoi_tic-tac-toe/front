export function isArray<T>(array: T[]): boolean {
  return Array.isArray(array);
}
