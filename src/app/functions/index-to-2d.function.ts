import { Vector2 } from 'three';

export function indexTo2D(index: number, arrayWidth: number): Vector2 {
  return new Vector2(
    index % arrayWidth,
    Math.floor(index / arrayWidth)
  );
}
