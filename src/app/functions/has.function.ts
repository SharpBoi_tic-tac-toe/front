export function hasField(obj: object, fieldName: string): boolean {
  return Object.keys(obj).includes(fieldName);
}
