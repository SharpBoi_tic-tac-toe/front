import { History as HistoryTyping, Location as LocationTyping } from 'history';
import { Store } from 'redux';
import { allRoutes } from '../constants/routes.const';
import { IRoute } from '../interfaces/route.interface';
import { AppState } from '../store';
import { navigateTo } from '../store/navigation/actions';
import { isNil } from './isNil.function';


export function syncRouteState(appHistory: HistoryTyping, appStore: Store<AppState>): HistoryTyping {
  const startupRoute = findRouteOfLocation(appHistory.location);
  appStore.dispatch(navigateTo(startupRoute));

  appHistory.listen((newLocation: LocationTyping) => {
    const routeObject = findRouteOfLocation(newLocation);
    appStore.dispatch(navigateTo(routeObject));
  });

  return appHistory;
}

function findRouteOfLocation(someLocation: LocationTyping): IRoute {
  const routeObject = allRoutes.find(route => route.route === someLocation.pathname);
  if (isNil(routeObject)) {
    throw new Error('unknown route path. TODO add 404 stub page');
  }
  return routeObject;
}
