export function isNumber<T>(value: T): boolean {
  return Number.isFinite(value);
}
