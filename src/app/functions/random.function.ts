import { isNil } from './isNil.function';

export function rnd(max: number): number;
export function rnd(min: number, max: number): number;
export function rnd(min: number, max?: number): number {
  if (isNil(max)) {
    return Math.random() * min;
  }

  return min + Math.random() * (max - min);
}
