import { useCallback, useEffect, useMemo } from 'react';
import { Observable, Subscription } from 'rxjs';

export function useSubscribe<T>(
  observable: Observable<T>,
  subscriptionCallback: (value: T) => void,
  deps: React.DependencyList
): void {
  const memoizedCallback = useCallback(subscriptionCallback, [...deps]);
  const subscription = useMemo(() => new Subscription(), [...deps]);

  useEffect(() => {
    subscription.add(observable.subscribe(memoizedCallback));

    return () => {
      subscription.unsubscribe();
    };
  }, [subscription, memoizedCallback]);
}
