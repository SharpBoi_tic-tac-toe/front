import { MonoTypeOperatorFunction } from 'rxjs';
import { filter } from 'rxjs/operators';

export function filterIsTrue<T extends Boolean>(): MonoTypeOperatorFunction<T> {
  return filter((value: T) => typeof value === 'boolean' && value);
}
