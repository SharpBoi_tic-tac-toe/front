import { MonoTypeOperatorFunction } from 'rxjs';
import { filter } from 'rxjs/operators';
import { isNotNil } from '../isNotNil.function';

export function filterNotNil<T>(): MonoTypeOperatorFunction<T> {
  return filter((value: T) => isNotNil(value));
}
