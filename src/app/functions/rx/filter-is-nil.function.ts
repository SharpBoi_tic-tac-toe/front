import { MonoTypeOperatorFunction } from 'rxjs';
import { filter } from 'rxjs/operators';
import { isNil } from '../isNil.function';

export function filterIsNil<T>(): MonoTypeOperatorFunction<T> {
  return filter((value: T) => isNil(value));
}
