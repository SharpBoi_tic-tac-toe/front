import { Vector2 } from 'three';

export function clipCoordinates(coordinates: Vector2, viewportWidth: number, viewportHeight: number): Vector2 {
  return new Vector2(
    coordinates.x / viewportWidth * 2 - 1,
    -(coordinates.y / viewportHeight * 2 - 1)
  );
}
