import { Object3D } from 'three';

export function traverseChildren(root: Object3D): Object3D[] {
  const children: Object3D[] = [];
  root.traverse(child => {
    children.push(child);
  });
  return children;
}

export function traverseType<T extends typeof Object3D>(root: Object3D, typo: T): InstanceType<T>[] {
  const children: InstanceType<T>[] = [];
  root.traverse(child => {
    if (child instanceof typo) {
      children.push(child as InstanceType<T>);
    }
  });
  return children;
}
