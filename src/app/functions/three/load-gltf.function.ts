import { GLTF, GLTFLoader } from '../../../reexports/gltf-loader';
import { ASSETS_3D_ROOT } from '../../constants/assets-paths.const';

const loader = new GLTFLoader();

export async function loadGLTF(gltfFile: string, onProgress?: (event: ProgressEvent) => void): Promise<GLTF> {
  const fullPath: string = `${ASSETS_3D_ROOT}/${gltfFile}`;

  return loader.loadAsync(fullPath, onProgress) as Promise<GLTF>;
}
