import { BufferGeometry, Geometry, Material, Mesh, MeshPhysicalMaterial, Scene, Texture } from 'three';
import { isNil } from '../isNil.function';

export function disposeScene(scene: Scene): void {
  if (isNil(scene)) {
    return;
  }

  scene.traverse(object3d => {
    if (object3d instanceof Mesh) {
      disposeGeometry(object3d.geometry);
      disposeMaterial(object3d.material);
    }
  });
}


function disposeGeometry(geometry: Geometry | BufferGeometry): void {
  if (isNil(geometry)) {
    return;
  }

  geometry.dispose();
}

function disposeMaterial(material: Material): void {
  if (isNil(material)) {
    return;
  }

  const materialWithMap: MeshPhysicalMaterial = material as MeshPhysicalMaterial;
  disposeTexture(materialWithMap.map);
  disposeTexture(materialWithMap.aoMap);
  disposeTexture(materialWithMap.metalnessMap);
  disposeTexture(materialWithMap.alphaMap);
  disposeTexture(materialWithMap.normalMap);
  disposeTexture(materialWithMap.emissiveMap);
  disposeTexture(materialWithMap.roughnessMap);
  material.dispose();
}

function disposeTexture(texture: Texture): void {
  if (isNil(texture)) {
    return;
  }

  texture.dispose();
}
