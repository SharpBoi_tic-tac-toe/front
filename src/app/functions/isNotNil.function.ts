import { isNil } from './isNil.function';

export function isNotNil<T>(value: T): boolean {
  return !isNil(value);
}
