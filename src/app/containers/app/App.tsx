import React from 'react';
import { useSelector } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import backgroundSrc from '../../../assets/static/images/backgrounds/default.jpg';
import { AI_GAME_ROUTE, HOME_ROUTE } from '../../constants/routes.const';
import { AiGamePage } from '../../pages/ai-game-page/ai-game-page';
import { MainPage } from '../../pages/main-page/main-page';
import { AppState } from '../../store';
import { UpdateRequestLayout } from '../Layouts/UpdateRequstLayout';
import style from './app.module.css';

export const App: React.FC<unknown> = () => {
  const routeTitle = useSelector((state: AppState) => state.navigation.currentRoute.routeTitle);

  return (
    <div className={style.app}>
      <title>{routeTitle}</title>
      <img src={backgroundSrc} className={style.background} />

      <Switch>
        <Route path={AI_GAME_ROUTE.route} component={AiGamePage}/>
        <Route path={HOME_ROUTE.route} component={MainPage} />
      </Switch>

      <UpdateRequestLayout/>
    </div>);
};

