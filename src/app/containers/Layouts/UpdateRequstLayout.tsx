import React, { useCallback, useState } from 'react';
import { map } from 'rxjs/operators';
import { Menu } from '../../components/menu/menu';
import { MenuItem } from '../../components/menu/menu-item/menu-item';
import { getInject } from '../../constants/injectors.const';
import { useSubscribe } from '../../functions/hooks/use-subscribe.function';
import { LocalStorageService } from '../../services/local-storage.service';
import { SWManagerService } from '../../services/sw-manager.service';
import { AppSettingsStorage, appSettingsStorageKey } from '../../storage/app-settings.storage';
import style from './layout.module.scss';

export const UpdateRequestLayout: React.FC = () => {
  const swManger = getInject(SWManagerService);
  const storageService = getInject(LocalStorageService);
  const [showUpdateNotif, setShowUpdateNotif] = useState(false);

  useSubscribe(swManger.onSWUpdated$.pipe(
    map(() => storageService.getValueKey<AppSettingsStorage>(
      appSettingsStorageKey,
      'autoUpdateWithoutAskUser'))
  ), autoUpdateEnabled => {
    if (autoUpdateEnabled) {
      update();
      return;
    }
    setShowUpdateNotif(true);
  }, []);
  const update = useCallback(() => window.location.reload(), []);
  const setAutoUpdateAndUpdate = useCallback(() => {
    storageService.setValueKey<AppSettingsStorage>(appSettingsStorageKey, { autoUpdateWithoutAskUser: true });
    update();
  }, []);

  return (
    <div className={style.layouts_container}>

      <div className={`${style.notification_container} ${showUpdateNotif && style.show}`}>
        <Menu title={'Update detected'}>
          <MenuItem onClick={update}>{'Update'}</MenuItem>
          <MenuItem onClick={setAutoUpdateAndUpdate}>{'Update automatically'}</MenuItem>
        </Menu>
      </div>
    </div>
  );
};
