import { IRoute } from '../interfaces/route.interface';

export const HOME_ROUTE: IRoute = {
  route: '/',
  routeTitle: 'Tic tac toe'
};
export const AI_GAME_ROUTE: IRoute = {
  route: '/ai-game',
  routeTitle: 'Play with computer'
};
export const ONLINE_GAME_ROUTE: IRoute = {
  route: '/online-game',
  routeTitle: 'Play online'
};

export const allRoutes: IRoute[] = [
  HOME_ROUTE,
  AI_GAME_ROUTE,
  ONLINE_GAME_ROUTE
];
