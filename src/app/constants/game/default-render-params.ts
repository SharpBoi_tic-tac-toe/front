import { WebGLRendererParameters } from 'three';

export const defaultRenderParams: WebGLRendererParameters = {
  alpha: true,
  antialias: true,
  powerPreference: 'default',
  precision: 'highp'
};
