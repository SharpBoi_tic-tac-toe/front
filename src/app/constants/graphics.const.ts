export const shadowBias: number = -0.00004;
export const gammaFactor: number = 2.2;
export const shadowMapSize: number = 1024;
