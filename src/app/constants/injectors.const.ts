import { createInjector } from '../modules/injector/create-injector';
import { InjectionContainer } from '../modules/injector/injection-container';

export const { inject, getInject, injectable, forceImportInjectable } = createInjector(new InjectionContainer());
