import { createBrowserHistory, History as HistoryTyping } from 'history';
import { HOME_ROUTE } from './routes.const';

export const appHistory: HistoryTyping = createBrowserHistory({
  basename: HOME_ROUTE.route
});
