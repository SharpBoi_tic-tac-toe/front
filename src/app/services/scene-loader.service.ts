import { BehaviorSubject } from 'rxjs';
import { take } from 'rxjs/operators';
import { SceneContext } from '../classes/scene-context.class';
import { inject, injectable } from '../constants/injectors.const';
import { loadGLTF } from '../functions/three/load-gltf.function';
import { SceneContextCacheService } from './scene-context-cache.service';

@injectable()
export class SceneLoaderService {
  @inject(SceneContextCacheService) private readonly sceneCache: SceneContextCacheService;

  public readonly loadingProgress$: BehaviorSubject<number> = new BehaviorSubject(0);

  public loadScene(sceneUrl: string): void {
    this.sceneCache.hasElement$(sceneUrl)
      .pipe(take(1))
      .subscribe(hasScene => {
        if (hasScene) {
          return;
        }

        loadGLTF(sceneUrl, loadEvent => this.loadingProgress$.next(loadEvent.loaded / loadEvent.total))
          .then(gltf => {
            const sceneContext: SceneContext = new SceneContext(gltf, sceneUrl);

            this.sceneCache.setElement(sceneUrl, sceneContext);
            return gltf;
          })
          .catch((err: Error) => err);
      });
  }
}
