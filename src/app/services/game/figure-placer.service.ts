import { Observable, Subject, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Object3D } from 'three';
import { CellSetBy } from '../../../types/game/cell-set-by.type';
import { GameFieldModel } from '../../classes/game-classes/game-field-model.class';
import { PrefabComposer } from '../../classes/prefab-composer.class';
import { injectable } from '../../constants/injectors.const';
import { ObjectsTags } from '../../enums/objects-tags';
import { filterNotNil } from '../../functions/rx/filter-not-nil.function';
import { CellData } from '../../interfaces/game/cell-data.interface';
import { appStore$ } from '../../store/app-store';
import { IGameState } from '../../store/game/state';

@injectable()
export class FigurePlacerService {
  public onPlaceFigure$: Subject<Object3D> = new Subject();

  private readonly gameState$: Observable<IGameState> = appStore$.pipe(map(state => state.game));
  private prefabComposer: PrefabComposer;
  private tableModel: GameFieldModel;

  constructor() {
    this.handleFigurePlacing();
  }

  public setSceneEnvironment(prefabComposer: PrefabComposer, tableModel: GameFieldModel): void {
    this.prefabComposer = prefabComposer;
    this.tableModel = tableModel;
  }

  private handleFigurePlacing(): Subscription {
    return this.gameState$
      .pipe(
        filter(state => state.status === 'process'),
        map(state => this.getLastChangedCellData(state)),
        filter(cellData => cellData.setBy !== 'free'),
        filterNotNil()
      )
      .subscribe(cellData => {
        const figure = this.getFigureByCellSetBy(cellData.setBy);

        const cell = this.tableModel.getCellByIndex(cellData.index);

        cell.add(figure);
        figure.position.set(0, 0, 0);

        this.onPlaceFigure$.next(figure);
      });
  }

  private getLastChangedCellData(gameState: IGameState): CellData {
    const cellsCopy: CellData[] = [...gameState.cells];
    return cellsCopy.sort((a, b) => a.changeOrder - b.changeOrder).pop();
  }
  private getFigureByCellSetBy(setBy: CellSetBy): Object3D {
    if (setBy === 'self') {
      return this.prefabComposer.instancePrefab(ObjectsTags.FigureX);
    }

    if (setBy === 'opponent') {
      return this.prefabComposer.instancePrefab(ObjectsTags.FigureO);
    }
  }
}
