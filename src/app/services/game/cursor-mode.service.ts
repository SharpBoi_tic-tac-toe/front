import { combineLatest, Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { CursorType } from '../../../types/cursor.type';
import { inject, injectable } from '../../constants/injectors.const';
import { isNil } from '../../functions/isNil.function';
import { appStore$ } from '../../store/app-store';
import { IGameState } from '../../store/game/state';
import { CellDetectorService } from './cell-detector.service';

@injectable()
export class CursorModeService {
  @inject(CellDetectorService) private readonly cellDetector: CellDetectorService;

  private readonly gameState$: Observable<IGameState> = appStore$.pipe(map(state => state.game));

  constructor() {
    this.handleCursorMode();
  }

  public setCursorType(cursor: CursorType): void {
    const bodySyle = document.body.style;
    bodySyle.cursor = cursor;
  }

  private handleCursorMode(): Subscription {
    return combineLatest([this.gameState$, this.cellDetector.hoveredCellIndex$]).subscribe(([state, cellIndex]) => {
      this.setCursorType('default');

      if (isNil(cellIndex)) {
        return;
      }

      if (state.nextTurnBy !== 'self') {
        this.setCursorType('not-allowed');
        return;
      }

      const cellData = state.cells.find(cell => cellIndex.equals(cell.index));

      if (cellData.setBy === 'free') {
        this.setCursorType('pointer');
        return;
      }

      this.setCursorType('not-allowed');
    });
  }
}
