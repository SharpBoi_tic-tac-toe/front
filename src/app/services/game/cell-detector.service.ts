import { Subject, Subscription } from 'rxjs';
import { Camera, Object3D, Raycaster, Vector2 } from 'three';
import { GameFieldModel } from '../../classes/game-classes/game-field-model.class';
import { injectable } from '../../constants/injectors.const';
import { isEmpty } from '../../functions/isEmpty.function';
import { filterNotNil } from '../../functions/rx/filter-not-nil.function';
import { ICanvasInput } from '../../interfaces/canvas-input.interface';

@injectable()
export class CellDetectorService {
  public readonly hoveredCellIndex$: Subject<Vector2> = new Subject();
  public readonly clickedCellIndex$: Subject<Vector2> = new Subject();

  private readonly raycast: Raycaster = new Raycaster();
  private subscription: Subscription = new Subscription();

  public setSceneEnvironment(
    tableModel: GameFieldModel,
    inputProvider: ICanvasInput,
    camera: Camera
  ): void {
    this.resetSubscription();

    this.subscription
      .add(
        inputProvider.onPointerMove.pipe(filterNotNil()).subscribe(pointer => {
          const cell = this.getCellUnderPointer(pointer, tableModel, camera);
          this.hoveredCellIndex$.next(tableModel.getIndexByCell(cell));
        })
      )
      .add(
        inputProvider.onPointerDown.pipe(filterNotNil()).subscribe(pointer => {
          const cell = this.getCellUnderPointer(pointer, tableModel, camera);
          this.clickedCellIndex$.next(tableModel.getIndexByCell(cell));
        })
      );
  }

  private resetSubscription(): void {
    this.subscription.unsubscribe();
    this.subscription = new Subscription();
  }

  private getCellUnderPointer(
    clipPointer: Vector2,
    tableModel: GameFieldModel,
    camera: Camera
  ): Object3D | null {
    this.raycast.setFromCamera(clipPointer, camera);
    const intersections = this.raycast.intersectObjects(tableModel.cells);

    if (!isEmpty(intersections)) {
      return [...intersections].reverse().pop().object;
    }

    return null;
  }
}
