import { Observable } from 'rxjs';
import { filter, first, map } from 'rxjs/operators';
import { Vector2 } from 'three';
import { PlayerType } from '../../../types/game/player.type';
import { injectable } from '../../constants/injectors.const';
import { appStore, appStore$ } from '../../store/app-store';
import { setGameCell } from '../../store/game/actions';
import { IGameState } from '../../store/game/state';

@injectable()
export class CellSetterService {
  private readonly gameState$: Observable<IGameState> = appStore$.pipe(map(state => state.game));

  public setCell(index: Vector2, player: PlayerType): void {
    this.gameState$
      .pipe(
        first(),
        filter(state => state.status === 'process'),
        filter(state => state.nextTurnBy === player),
      )
      .subscribe(state => {
        const clickedCellData = state.cells.find(cell => cell.index.equals(index));
        if (clickedCellData.setBy !== 'free') {
          return;
        }

        appStore.dispatch(setGameCell({
          setBy: player,
          index: clickedCellData.index,
          changeOrder: -1
        }));
      });
  }
}
