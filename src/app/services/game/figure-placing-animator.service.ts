import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { LoopOnce, Object3D } from 'three';
import { AnimationController } from '../../classes/game-classes/animation-controller.class';
import { inject, injectable } from '../../constants/injectors.const';
import { AnimationsNames } from '../../enums/animations-names';
import { ObjectsTags } from '../../enums/objects-tags';
import { hasField } from '../../functions/has.function';
import { filterNotNil } from '../../functions/rx/filter-not-nil.function';
import { traverseChildren } from '../../functions/three/traverse-children.function';
import { IDisposable } from '../../interfaces/disposable.interface';
import { FigurePlacerService } from './figure-placer.service';

@injectable()
export class FigurePlacingAnimatorService implements IDisposable {
  @inject(FigurePlacerService) private readonly figurePlacer: FigurePlacerService;

  private animationController: AnimationController;

  private subscription: Subscription = new Subscription();

  public setSceneEnvironment(animationController: AnimationController): void {
    this.resetSubscription();
    this.animationController = animationController;

    this.subscription.add(this.handlePlacingAnimation());
  }

  public dispose(): void {
    this.subscription.unsubscribe();
  }

  private handlePlacingAnimation(): Subscription {
    return this.figurePlacer.onPlaceFigure$
      .pipe(
        map(figure => this.getFigureModel(figure)),
        filterNotNil()
      )
      .subscribe(model => {
        const clip = this.animationController.getAnimationClip(AnimationsNames.PlaceFigure);
        const action = this.animationController.createIndividualAnimation(model, clip);
        action.loop = LoopOnce;
        action.clampWhenFinished = true;
        action.play();
      });
  }

  private getFigureModel(figure: Object3D): Object3D {
    return traverseChildren(figure).find(child => hasField(child.userData, ObjectsTags.FigureModel));
  }

  private resetSubscription(): void {
    this.subscription.unsubscribe();
    this.subscription = new Subscription();
  }
}
