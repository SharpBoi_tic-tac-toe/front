import anime from 'animejs';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { filter, map, withLatestFrom } from 'rxjs/operators';
import { Euler, LoopOnce, Object3D, Quaternion, Vector2 } from 'three';
import { AnimationController } from '../../classes/game-classes/animation-controller.class';
import { inject, injectable } from '../../constants/injectors.const';
import { AnimationsNames } from '../../enums/animations-names';
import { filterIsTrue } from '../../functions/rx/filter-is-true.function';
import { filterNotNil } from '../../functions/rx/filter-not-nil.function';
import { ICanvasInput } from '../../interfaces/canvas-input.interface';
import { appStore$ } from '../../store/app-store';
import { IGameState } from '../../store/game/state';
import { CellDetectorService } from './cell-detector.service';

@injectable()
export class TableAnimatorService {
  @inject(CellDetectorService) private readonly cellDetector: CellDetectorService;
  private readonly gameState$: Observable<IGameState> = appStore$.pipe(map(state => state.game));
  private readonly isGameInProcess$: Observable<boolean> = this.gameState$.pipe(
    map(state => state.status === 'process')
  );
  private readonly isGameEnd$: Observable<boolean> = this.gameState$.pipe(
    map(state => state.status === 'end')
  );

  private subscription: Subscription = new Subscription();
  private tablePivot: Object3D;
  private input: ICanvasInput;
  private animController: AnimationController;

  private readonly pointerPositionRelax: number = 0.25;
  private readonly rotationTimeScale: number = 4;
  private readonly tableClickOffset: number = 0.1;
  private readonly tableClickAnimationDuration: number = 125;
  private readonly targetRotation: Quaternion = new Quaternion();
  private readonly clippedPointer: Vector2 = new Vector2();
  private isGameProcess: boolean = true;
  private isPointerOut: boolean = true;
  private clickAnimation: anime.AnimeInstance = anime({});

  public setSceneEnvironment(
    tablePivot: Object3D,
    input: ICanvasInput,
    animController: AnimationController
  ): void {
    this.isGameProcess = true;
    this.tablePivot = tablePivot;
    this.input = input;
    this.animController = animController;

    this.resetSubscription();

    this.subscription
      .add(this.handleGetPointerPosition())
      .add(this.handleIsCanRotate())
      .add(this.handleClickAnimation())
      .add(this.handleEndAnimation());
  }

  public update(deltaTime: number): void {
    this.targetRotation.identity();

    if (!this.isPointerOut) {
      this.targetRotation.setFromEuler(new Euler(-this.clippedPointer.y, this.clippedPointer.x));
    }

    if (this.isGameProcess) {
      this.tablePivot.quaternion.slerp(this.targetRotation, deltaTime * this.rotationTimeScale);
    }
  }

  private handleGetPointerPosition(): Subscription {
    return this.input.onPointerMove.pipe(filterNotNil()).subscribe(pointer => {
      this.clippedPointer.copy(pointer).multiplyScalar(this.pointerPositionRelax);
    });
  }
  private handleIsCanRotate(): Subscription {
    return combineLatest([this.isGameInProcess$, this.input.isPointerOut]).subscribe(
      ([isGameProcess, isPointerOut]) => {
        this.isPointerOut = isPointerOut;
        this.isGameProcess = isGameProcess;
      }
    );
  }
  private handleClickAnimation(): Subscription {
    return this.cellDetector.clickedCellIndex$
      .pipe(
        filterNotNil(),
        filter(() => this.clickAnimation?.completed),
        withLatestFrom(this.isGameInProcess$),
        filter(([_, isGameProcess]) => isGameProcess)
      )
      .subscribe(() => {
        this.runClickAnimation();
      });
  }

  private handleEndAnimation(): Subscription {
    return this.isGameEnd$.pipe(filterIsTrue()).subscribe(() => {
      this.clickAnimation.pause();

      const rotateTableAnim = this.animController.createLinkedAnimation(
        AnimationsNames.TurnTableBackwards
      );
      const showEmlemAnim = this.animController.createLinkedAnimation(AnimationsNames.ShowEmblem);

      rotateTableAnim.loop = LoopOnce;
      rotateTableAnim.clampWhenFinished = true;

      showEmlemAnim.loop = LoopOnce;
      showEmlemAnim.clampWhenFinished = true;

      rotateTableAnim.play();
      showEmlemAnim.play();
    });
  }

  private runClickAnimation(): void {
    this.clickAnimation = anime({
      targets: this.tablePivot.position,
      z: this.tablePivot.position.z - this.tableClickOffset,
      easing: 'easeInOutSine',
      direction: 'alternate',
      loop: 1,
      duration: this.tableClickAnimationDuration
    });
  }

  private resetSubscription(): void {
    this.subscription.unsubscribe();
    this.subscription = new Subscription();
  }
}
