import { BaseCache } from '../classes/base-cache.class';
import { injectable } from '../constants/injectors.const';

@injectable()
export class CanvasCacheService extends BaseCache<HTMLCanvasElement> {}
