/* eslint-disable @typescript-eslint/indent */
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { MessageCreator } from '../../../service-worker/functions/create-message.function';
import { SWMessageData } from '../../../service-worker/interfaces/sw-message.interface';
import { SWRegisterResult } from '../../types/sw-register-result.type';
import { injectable } from '../constants/injectors.const';
import { isNotNil } from '../functions/isNotNil.function';

@injectable()
export class SWManagerService {
  public readonly onSWUpdated$ = new Subject();
  public readonly onSWFirstInstall$ = new Subject();
  public readonly registerResult$ = new BehaviorSubject<SWRegisterResult>('pending');

  private readonly swUrl: string = 'service-worker.js';
  private readonly onMessageSubject$: Subject<SWMessageData<any>> = new Subject();
  private readonly registration$: BehaviorSubject<ServiceWorkerRegistration> = new BehaviorSubject(
    null
  );

  constructor() {
    this.handleReceiveMessages();
  }

  public async register(): Promise<SWRegisterResult> {
    if (!('serviceWorker' in navigator)) {
      console.log('sw is not supported');
      this.registerResult$.next('not_supported');
      return 'not_supported';
    }

    try {
      this.handleSWUpdateCheck();

      const registration = await navigator.serviceWorker.register(this.swUrl);
      this.registration$.next(registration);
      this.registerResult$.next('success');

      await this.handleSWEvents(registration);

      console.log('SW register success');
      return 'success';
    } catch (e) {
      console.log('SW register went wrong', e);
      this.registerResult$.next('error');
      return 'error';
    }
  }

  public postMessage<T extends SWMessageData<unknown>>(message: T): void {
    const registration = this.registration$.getValue();
    registration.active.postMessage(message);
  }
  public onMessage$<T>(message: MessageCreator<T>): Observable<SWMessageData<T>> {
    return this.onMessageSubject$.pipe(
      filter((messageSubject: SWMessageData<T>) => messageSubject.type === message.type)
    );
  }

  private async handleSWEvents(registration: ServiceWorkerRegistration): Promise<void> {
    const { installing } = registration;

    if (isNotNil(installing)) {
      await this.awaitSWStateChange(installing, ['activated']);
      this.onSWFirstInstall$.next();
    }
  }

  private async awaitSWStateChange(
    sw: ServiceWorker,
    resolveOnState: ServiceWorkerState[] | 'any_state'
  ): Promise<ServiceWorkerState> {
    return new Promise(resolve => {
      sw.addEventListener('statechange', () => {
        const { state } = sw;

        if (resolveOnState === 'any_state') {
          return resolve(state);
        }

        if (resolveOnState.includes(state)) {
          return resolve(state);
        }
      });
    });
  }

  private handleSWUpdateCheck(): void {
    navigator.serviceWorker.addEventListener('controllerchange', () => {
      this.onSWUpdated$.next();
    });
  }
  private handleReceiveMessages(): void {
    navigator.serviceWorker.addEventListener('message', e => {
      const { data }: { data: SWMessageData<unknown> } = e;
      this.onMessageSubject$.next(data);
    });
  }
}
