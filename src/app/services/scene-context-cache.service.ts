import { BaseCache } from '../classes/base-cache.class';
import { SceneContext } from '../classes/scene-context.class';
import { injectable } from '../constants/injectors.const';

@injectable()
export class SceneContextCacheService extends BaseCache<SceneContext> { }
