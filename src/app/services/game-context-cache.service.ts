import { BaseCache } from '../classes/base-cache.class';
import { BasicGameContext } from '../classes/basic-game-context.class';
import { injectable } from '../constants/injectors.const';

@injectable()
export class GameContextCacheService extends BaseCache<BasicGameContext> {}
