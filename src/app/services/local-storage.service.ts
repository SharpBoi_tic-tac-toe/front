import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { injectable } from '../constants/injectors.const';
import { isNil } from '../functions/isNil.function';
import { isNotNil } from '../functions/isNotNil.function';

@injectable()
export class LocalStorageService {
  public readonly storage = window.localStorage;
  public readonly onStorageChange$ = new BehaviorSubject(this.getStorageContent());

  constructor() {
    this.handleStorageChangeDetect();
  }

  public initializeValue<T>(key: string, initialValue: T): void {
    const value = this.getValue(key);
    if (isNotNil(value)) {
      return;
    }

    this.setValue(key, initialValue);
  }

  public setValue<T>(key: string, value: T | string): void {
    if (typeof value === 'string') {
      this.storage.setItem(key, value);
    }

    this.storage.setItem(key, JSON.stringify(value));
  }
  public setValueKey<T>(key: string, valueKeys: Partial<T>): void {
    const value: T = this.getParsedValue(key);
    const newValue = {
      ...value,
      ...valueKeys
    };
    this.setValue(key, newValue);
  }

  public getValue(key: string): string {
    const valueString = this.storage.getItem(key);
    return valueString;
  }
  public getParsedValue<T>(key: string): T {
    const parsedValue: T = JSON.parse(this.getValue(key));
    return parsedValue;
  }
  public getValueKey<T, K extends keyof T = keyof T>(key: string, valueKey: K): T[K] {
    const value = this.getParsedValue<T>(key);

    if (isNil(value)) {
      return null;
    }

    return value[valueKey];
  }

  public getValueOnChange$(key: string): Observable<string> {
    return this.onStorageChange$.pipe(map(contentMap => contentMap.get(key)));
  }
  public getParsedValueOnChange$<T>(key: string): Observable<T> {
    return this.getValueOnChange$(key).pipe(
      map(value => {
        const parsedValue: T = JSON.parse(value);
        return parsedValue;
      })
    );
  }

  private handleStorageChangeDetect(): void {
    window.addEventListener('storage', e => {
      if (e.storageArea !== this.storage) {
        return;
      }

      this.onStorageChange$.next(this.getStorageContent());
    });
  }
  private getStorageContent(): Map<string, string> {
    const contentMap = new Map<string, string>();
    const keys = new Array<String>(this.storage.length)
      .fill(null)
      .map((_, index) => this.storage.key(index));

    for (const key of keys) {
      const value = this.getValue(key);
      contentMap.set(key, value);
    }

    return contentMap;
  }
}
