import { BaseCache } from '../classes/base-cache.class';
import { RenderGenerator } from '../classes/render-generator.class';
import { injectable } from '../constants/injectors.const';

@injectable()
export class RenderGeneratorCacheService extends BaseCache<RenderGenerator> {}
