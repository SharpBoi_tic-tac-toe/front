import { Vector2 } from 'three';
import { CellSetBy } from '../../../types/game/cell-set-by.type';

export interface CellData {
  index: Vector2;
  setBy: CellSetBy;
  changeOrder: number;
}
