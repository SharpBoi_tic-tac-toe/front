import { Subject } from 'rxjs';
import { Vector2 } from 'three';
import { IDisposable } from '../disposable.interface';

export interface IPlayer extends IDisposable {
  onSetCell$: Subject<Vector2>;
}
