import { Subject } from 'rxjs';
import { Vector2 } from 'three';
import { IDisposable } from './disposable.interface';

export interface ICanvasInput extends IDisposable {
  onPointerDown: Subject<Vector2>;
  onPointerMove: Subject<Vector2>;
  onPointerUp: Subject<Vector2>;
  onPointerLeave? : Subject<void>;

  isPointerOut?: Subject<boolean>;
}
