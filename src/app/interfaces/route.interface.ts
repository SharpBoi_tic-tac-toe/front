export interface IRoute {
  route: string;
  routeTitle: string;
}
