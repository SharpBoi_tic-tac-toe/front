import { Reducer } from 'redux';
import { CellSetBy } from '../../../types/game/cell-set-by.type';
import { NextTurnBy } from '../../../types/game/next-turn-by.type';
import { CellData } from '../../interfaces/game/cell-data.interface';
import { createReducer } from '../../store-helpers/create-reducer';
import { restoreGame, setGameCell, setGameMode, setGameNextTurnPlayer, setGameStatus, setGameWinner } from './actions';
import { IGameState, initialGameState } from './state';

export const gameStateReducer: Reducer<IGameState> = createReducer(initialGameState)
  .handleAction(setGameCell, (state, action) => {
    const newCellData: CellData = {
      ...action.payload,
      index: action.payload.index.clone()
    };
    const stateCells: CellData[] = state.cells;

    const cellIndexToUpdate = stateCells.findIndex(cell => cell.index.equals(newCellData.index));
    const cellToUpdate = stateCells[cellIndexToUpdate];

    if (cellToUpdate.setBy !== 'free') {
      return state;
    }

    const maxChangeOrder: number = Math.max(...stateCells.map(cell => cell.changeOrder));
    newCellData.changeOrder = maxChangeOrder + 1;

    const newStateCells = stateCells.map(cell => ({
      ...cell,
      index: cell.index.clone()
    }));
    newStateCells[cellIndexToUpdate] = newCellData;

    const nextTurnBy: NextTurnBy = getOppositePlayer(newCellData.setBy);

    return {
      ...state,
      cells: newStateCells,
      nextTurnBy
    };
  })
  .handleAction(setGameStatus, (state, action) => ({
    ...state,
    status: action.payload
  }))
  .handleAction(setGameNextTurnPlayer, (state, action) => ({
    ...state,
    nextTurnBy: action.payload
  }))
  .handleAction(setGameWinner, (state, action) => ({
    ...state,
    winner: action.payload
  }))
  .handleAction(setGameMode, (state, action) => ({
    ...state,
    gameMode: action.payload
  }))
  .handleAction(restoreGame, () => ({
    ...initialGameState
  }))
  .mixHandlers();

function getOppositePlayer(setBy: CellSetBy): NextTurnBy {
  if (setBy === 'self') {
    return 'opponent';
  }

  if (setBy === 'opponent') {
    return 'self';
  }
}
