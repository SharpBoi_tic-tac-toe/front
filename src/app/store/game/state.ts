import { GameMode } from '../../../types/game/game-mode.type';
import { GameStatus } from '../../../types/game/game-status.type';
import { NextTurnBy } from '../../../types/game/next-turn-by.type';
import { WinnerType } from '../../../types/game/winner.type';
import { indexTo2D } from '../../functions/index-to-2d.function';
import { CellData } from '../../interfaces/game/cell-data.interface';

const fieldSize: number = 9;
const fieldWidth: number = 3;

export interface IGameState {
  status: GameStatus;
  winner: WinnerType;
  nextTurnBy: NextTurnBy;
  gameMode: GameMode;
  cells: CellData[];
}

export const initialGameState: IGameState = {
  status: 'not_created',
  winner: 'unknown',
  nextTurnBy: 'unknown',
  gameMode: 'unknown',
  cells: new Array(fieldSize).fill(null).map((_, index) => {
    const cellData: CellData = {
      setBy: 'free',
      index: indexTo2D(index, fieldWidth),
      changeOrder: index
    };
    return cellData;
  })

};
