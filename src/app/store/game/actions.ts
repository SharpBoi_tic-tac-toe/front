import { GameMode } from '../../../types/game/game-mode.type';
import { GameStatus } from '../../../types/game/game-status.type';
import { PlayerType } from '../../../types/game/player.type';
import { WinnerType } from '../../../types/game/winner.type';
import { CellData } from '../../interfaces/game/cell-data.interface';
import { actionType } from '../../store-helpers/action-type';
import { ActionCreator, createAction } from '../../store-helpers/create-action';

const section: string = 'game';
export const setGameCell: ActionCreator<CellData> = createAction(actionType(section, 'SET_CELL'));
export const setGameStatus: ActionCreator<GameStatus> = createAction(actionType(section, 'SET_STATUS'));
export const setGameNextTurnPlayer: ActionCreator<PlayerType> = createAction(actionType(section, 'SET_NEXT_TURN'));
export const setGameWinner: ActionCreator<WinnerType> = createAction(actionType(section, 'SET_WINNER'));
export const setGameMode: ActionCreator<GameMode> = createAction(actionType(section, 'SET_GAME_MODE'));
export const restoreGame: ActionCreator = createAction(actionType(section, 'RESTORE_GAME'));
