import { Reducer } from 'redux';
import { createReducer } from '../../store-helpers/create-reducer';
import { navigateTo } from './actions';
import { INavigationState, initialNavigationState } from './state';

export const navigationReducer: Reducer<INavigationState> =
  createReducer(initialNavigationState)
    .handleAction(navigateTo, (state, action) => ({
      ...state,
      currentRoute: action.payload
    }))
    .mixHandlers();
