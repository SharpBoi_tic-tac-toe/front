import { IRoute } from '../../interfaces/route.interface';
import { actionType } from '../../store-helpers/action-type';
import { ActionCreator, createAction } from '../../store-helpers/create-action';

const sectionName: string = 'navigation';

export const navigateTo: ActionCreator<IRoute> =
  createAction(actionType(sectionName, 'NAVIGATE_TO'));

