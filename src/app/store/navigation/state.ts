import { HOME_ROUTE } from '../../constants/routes.const';
import { IRoute } from '../../interfaces/route.interface';

export interface INavigationState {
  currentRoute: IRoute;
}

export const initialNavigationState: INavigationState = {
  currentRoute: HOME_ROUTE
};
