import { combineReducers, Reducer } from 'redux';
import { gameStateReducer } from './game/reducer';
import { IGameState } from './game/state';
import { navigationReducer } from './navigation/reducer';
import { INavigationState } from './navigation/state';

export interface AppState {
  navigation: INavigationState;
  game: IGameState;
}

export const rootReducer: Reducer<AppState> = combineReducers<AppState>({
  navigation: navigationReducer,
  game: gameStateReducer
});
