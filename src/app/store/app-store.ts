import { compose, createStore, Store } from 'redux';
import { BehaviorSubject } from 'rxjs';
import { AppState, rootReducer } from '.';
import { isNotNil } from '../functions/isNotNil.function';


export const appStore: Store<AppState> =
  createStore(rootReducer,
    isNotNil(window.__REDUX_DEVTOOLS_EXTENSION__) &&
      compose(window.__REDUX_DEVTOOLS_EXTENSION__())
  );


export const appStore$: BehaviorSubject<AppState> = new BehaviorSubject(appStore.getState());
appStore.subscribe(() => {
  appStore$.next(appStore.getState());
});
