import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { combineLatest } from 'rxjs';
import { GameStatus } from '../../../types/game/game-status.type';
import { MainGameContext } from '../../classes/game-classes/main-game-context.class';
import { RenderGenerator } from '../../classes/render-generator.class';
import { SceneContext } from '../../classes/scene-context.class';
import { View3D } from '../../components/view-3d/view-3d';
import { defaultRenderParams } from '../../constants/game/default-render-params';
import { getInject } from '../../constants/injectors.const';
import { filterNotNil } from '../../functions/rx/filter-not-nil.function';
import { CanvasCacheService } from '../../services/canvas-cache.service';
import { GameContextCacheService } from '../../services/game-context-cache.service';
import { RenderGeneratorCacheService } from '../../services/render-generator-cache.service';
import { SceneContextCacheService } from '../../services/scene-context-cache.service';
import { SceneLoaderService } from '../../services/scene-loader.service';
import { AppState } from '../../store';
import style from './base-game-page.module.scss';

type Props = {
  sceneUrl: string;
  onContentLoaded: VoidFunction;
};
export const BaseGamePage: React.FC<Props> = props => {
  const sceneCtxCache: SceneContextCacheService = getInject(SceneContextCacheService);
  const canvasCache: CanvasCacheService = getInject(CanvasCacheService);

  const gameStatus = useSelector<AppState, GameStatus>(state => state.game.status);
  const [is3DLoaded, setIs3DLoaded] = useState(false);

  const { sceneUrl, onContentLoaded } = props;
  const needLoadContent = gameStatus === 'loadingContent';

  useEffect(() => {
    if (!needLoadContent) {
      return;
    }

    setIs3DLoaded(false);
    unload3DResources(sceneUrl);
    load3DResources(sceneUrl);

  }, [needLoadContent, sceneUrl]);

  useEffect(() => {
    const subscription = combineLatest([
      canvasCache.getElement$(sceneUrl).pipe(filterNotNil()),
      sceneCtxCache.getElement$(sceneUrl).pipe(filterNotNil())
    ]).subscribe(([canvas, sceneCtx]) => {
      spawnGameInstance(sceneCtx, canvas, sceneUrl);

      setIs3DLoaded(true);
      onContentLoaded();
    });

    return () => {
      subscription.unsubscribe();
      unload3DResources(sceneUrl);
    };
  }, []);

  return (
    <div className={style.base_game_page}>

      <View3D canvasId={sceneUrl} isLoaded={is3DLoaded} />
    </div>
  );
};


function unload3DResources(sceneUrl: string): void {
  const renderCache: RenderGeneratorCacheService = getInject(RenderGeneratorCacheService);
  const gameCtxCache: GameContextCacheService = getInject(GameContextCacheService);
  const sceneCtxCache: SceneContextCacheService = getInject(SceneContextCacheService);

  const renderer = renderCache.getElement(sceneUrl);
  renderer?.dispose();
  renderCache.removeElement(sceneUrl);

  const gameCtx = gameCtxCache.getElement(sceneUrl);
  gameCtx?.dispose();
  gameCtxCache.removeElement(sceneUrl);

  const sceneCtx = sceneCtxCache.getElement(sceneUrl);
  sceneCtx?.dispose();
  sceneCtxCache.removeElement(sceneUrl);
}
function load3DResources(sceneUrl: string): void {
  const sceneCtxCache: SceneContextCacheService = getInject(SceneContextCacheService);
  const sceneLoader: SceneLoaderService = getInject(SceneLoaderService);

  const oldScene = sceneCtxCache.getElement(sceneUrl);
  oldScene?.dispose();
  sceneCtxCache.removeElement(sceneUrl);
  sceneLoader.loadScene(sceneUrl);
}

function spawnGameInstance(sceneCtx: SceneContext, canvas: HTMLCanvasElement, sceneUrl: string): void {
  const renderCache: RenderGeneratorCacheService = getInject(RenderGeneratorCacheService);
  const gameCtxCache: GameContextCacheService = getInject(GameContextCacheService);

  const renderer = new RenderGenerator(sceneCtx, canvas, defaultRenderParams);
  renderer.startRenderLoop();
  renderCache.setElement(sceneUrl, renderer);

  const mainGameCtx = new MainGameContext(sceneCtx, renderer);
  gameCtxCache.setElement(sceneUrl, mainGameCtx);
}
