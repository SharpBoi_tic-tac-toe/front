import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import reloadImg from '../../../assets/static/images/reload.svg';
import { PlayerType } from '../../../types/game/player.type';
import { WinnerType } from '../../../types/game/winner.type';
import { Header } from '../../components/header/header';
import { appHistory } from '../../constants/history.const';
import { HOME_ROUTE } from '../../constants/routes.const';
import { DEFAULT_SCENE_URL } from '../../constants/scenes.const';
import { rnd } from '../../functions/random.function';
import { AppState } from '../../store';
import { restoreGame, setGameMode, setGameNextTurnPlayer, setGameStatus } from '../../store/game/actions';
import { BaseGamePage } from '../base-game-page/base-game-page';
import style from './ai-game-page.module.scss';

export const AiGamePage: React.FC = () => {

  const dispatch = useDispatch();
  const setupGameState = useCallback(() => {
    const firstTurn = getRandomTurn();
    dispatch(restoreGame());
    dispatch(setGameStatus('loadingContent'));
    dispatch(setGameNextTurnPlayer(firstTurn));
    dispatch(setGameMode('ai'));

  }, []);
  const onContentLoaded = useCallback(() => {
    dispatch(setGameStatus('process'));
  }, []);

  useEffect(() => {
    setupGameState();
  }, []);

  const isGameEnd = useSelector<AppState, boolean>(state => state.game.status === 'end');
  const winnerTitle = useSelector<AppState, string>(state => getWinnerTitle(state.game.winner));

  return (
    <div className={style.ai_game_page}>
      <Header title='Game with ai' onClickBackArrow={handleBackwardClick} />
      {isGameEnd &&
      <div className={style.end_game_title_wrapper}>
        <div className={style.end_game_title}>
          {winnerTitle}
        </div>
      </div>}
      <div className={style.content_wrapper}>
        <BaseGamePage sceneUrl={DEFAULT_SCENE_URL} onContentLoaded={onContentLoaded} />

        <div className={`${style.reload_wrapper} ${isGameEnd && style.show_reloader}`}>
          <img src={reloadImg} onClick={setupGameState}/>
        </div>
      </div>
    </div>
  );
};
function handleBackwardClick(): void {
  appHistory.push(HOME_ROUTE.route);
}
function getRandomTurn(): PlayerType {
  const delimiter = 0.5;
  if (rnd(1) < delimiter) {
    return 'self';
  }

  return 'opponent';
}

function getWinnerTitle(winner: WinnerType): string {
  switch (winner) {
    case 'draw': return 'Draw !';
    case 'opponent': return 'You lose :(';
    case 'self': return 'You Win !';
  }
}
