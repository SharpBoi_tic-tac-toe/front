import React from 'react';
import { ContactsPopup } from '../../components/contacts-popup/ContactsPopup';
import { Menu } from '../../components/menu/menu';
import { MenuItem } from '../../components/menu/menu-item/menu-item';
import { appHistory } from '../../constants/history.const';
import { AI_GAME_ROUTE } from '../../constants/routes.const';
import style from './main-page.module.scss';

export const MainPage: React.FC = () => (
  <div className={style.container}>

    <ContactsPopup/>

    <div className={style.menu_wrappper}>
      <Menu title='Tic tac toe' footer='v0.1.2'>
        <MenuItem onClick={navToAiGame}>{'Play with ai'}</MenuItem>
        <MenuItem disable>{'Play online'}</MenuItem>
      </Menu>
    </div>
  </div>
);

function navToAiGame(): void {
  appHistory.push(AI_GAME_ROUTE.route);
}
