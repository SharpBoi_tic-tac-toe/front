export enum ObjectsTags {
  MainCamera = 'main-camera',
  TablePivot = 'table-pivot',
  Cell = 'cell',

  FigureX = 'figure-x',
  FigureO = 'figure-o',
  FigureModel = 'figure-model',

  EmblemPivot = 'emblem-pivot',
  WinEmblem = 'win-emblem',
  LoseEmblem = 'lose-emblem'
}
