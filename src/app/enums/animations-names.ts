export enum AnimationsNames {
  TurnTableBackwards = 'TurnTableBackwards',
  ShowEmblem = 'ShowEmblem',

  PlaceFigure = 'PlaceFigure'
}
