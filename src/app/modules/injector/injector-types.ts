export type Ctor<T> = new () => T;
export type InstanceResolver<T> = () => T;
export interface InjectionBundle<T> {
  typeCtor: Ctor<T>;
  instance: T;
}

export type VarInjector = <T>(ctor: Ctor<T>) => T;
export type InjectorPropertyDecorator = (target: Object, propertyKey: string) => void;
export type InjectableClassDecorator = <T>(target: Ctor<T>) => void;

export interface InjectCreator {
  inject: <T>(ctor: Ctor<T>) => InjectorPropertyDecorator;
  getInject: VarInjector;
  injectable: () => InjectableClassDecorator;
  forceImportInjectable: <T>(ctor: Ctor<T>) => void;
}
