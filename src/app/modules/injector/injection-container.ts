import { Ctor, InjectionBundle } from './injector-types';

export class InjectionContainer<Tinstance = unknown> {
  public readonly ctorInjectionMap: Map<Ctor<unknown>, InjectionBundle<Tinstance>> = new Map();

  public getInjectionBundle<T extends Tinstance>(ctor: Ctor<T>): InjectionBundle<T> {
    return this.ctorInjectionMap.get(ctor) as InjectionBundle<T>;
  }
  public setInjectionBundle<T extends Tinstance>(ctor: Ctor<T>, bundle: InjectionBundle<T>): void {
    this.ctorInjectionMap.set(ctor, bundle);
  }
  public getOrCreateInjectionBundle<T extends Tinstance>(ctor: Ctor<T>): InjectionBundle<T> {
    if (this.ctorInjectionMap.has(ctor)) {
      return this.getInjectionBundle(ctor);
    }

    const bundle: InjectionBundle<T> = this.createInjectionBundle(ctor);
    this.setInjectionBundle(ctor, bundle);
    return bundle;
  }

  public instantiateType<T>(ctor: Ctor<T>): T {
    return new ctor();
  }
  public createInjectionBundle<T>(ctor: Ctor<T>): InjectionBundle<T> {
    const typeInstance: T = this.instantiateType(ctor);
    const bundle: InjectionBundle<T> = {
      instance: typeInstance,
      typeCtor: ctor
    };
    return bundle;
  }
}
