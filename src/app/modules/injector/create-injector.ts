/* eslint-disable no-console */
import { InjectionContainer } from './injection-container';
import { Ctor, InjectableClassDecorator, InjectCreator, InjectorPropertyDecorator } from './injector-types';

export function createInjector(diContainer: InjectionContainer): InjectCreator {

  function injectable(): InjectableClassDecorator {
    return <T>(ctor: Ctor<T>) => {
      if (diContainer.ctorInjectionMap.has(ctor)) {
        throw new Error('Constructor for injection already registered');
      }

      const injection = diContainer.createInjectionBundle(ctor);
      diContainer.setInjectionBundle(ctor, injection);
    };
  }

  function inject<T>(ctor: Ctor<T>): InjectorPropertyDecorator {
    return (target: Object, propertyKey: string) => {
      let tempValue: T = diContainer.getOrCreateInjectionBundle(ctor).instance;

      Object.defineProperty(target, propertyKey, {
        get() {
          return tempValue;
        },
        set(value: T) {
          tempValue = value;
        }
      });
    };
  }

  function getInject<T>(ctor: Ctor<T>): T {
    return diContainer.getOrCreateInjectionBundle(ctor).instance;
  }

  function forceImportInjectable<T>(ctor: Ctor<T>): void {
    if (diContainer.ctorInjectionMap.has(ctor)) {
      return;
    }

    const injection = diContainer.createInjectionBundle(ctor);
    diContainer.setInjectionBundle(ctor, injection);
  }

  return {
    inject,
    getInject,
    injectable,
    forceImportInjectable
  };
}
