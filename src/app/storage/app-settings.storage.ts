export interface AppSettingsStorage {
  autoUpdateWithoutAskUser: boolean;
}

export const initialAppSettingsStorage: AppSettingsStorage = {
  autoUpdateWithoutAskUser: false
};

export const appSettingsStorageKey = 'app-settings';
