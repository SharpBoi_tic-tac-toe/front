const { webpackBaseConfig } = require('./webpack-base.config');
const TerserPlugin = require('terser-webpack-plugin');
const { createTsxConfig } = require('./pipeline/configs/tsx.config');
const { chainTransforms, chainPathTransforms } = require('./pipeline/functions/chain-transform');
const { textureSize } = require('./pipeline/transformers/texture-size.transform');
const { hashGltfPaths } = require('./pipeline/transformers/hash-gltf-paths.transform');
const { textureWebP } = require('./pipeline/transformers/texture-webp.transform');
const CopyPlugin = require('copy-webpack-plugin');
const { srcAssets3DPath, dstAssets3DPath } = require('./pipeline/consts/paths');

module.exports.webpackProdConfig = {
  ...webpackBaseConfig,
  module: {
    rules: [...webpackBaseConfig.module.rules, createTsxConfig('prod')]
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        {
          from: srcAssets3DPath,
          to: dstAssets3DPath,
          transform: chainTransforms([textureSize(1024), textureWebP(true)]),
          transformPath: chainPathTransforms([hashGltfPaths()])
        }
      ]
    }),
    ...webpackBaseConfig.plugins
  ],

  mode: 'production',
  devtool: 'none',
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        extractComments: false
      })
    ]
  }
};
